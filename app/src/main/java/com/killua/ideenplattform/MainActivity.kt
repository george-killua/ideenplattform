package com.killua.ideenplattform


import android.annotation.SuppressLint
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.view.MenuItem
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.navigation.NavController
import androidx.navigation.NavOptions
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.killua.ideenplattform.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {

    val binding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }

    @SuppressLint("SourceLockedOrientationActivity")
    override fun onCreate(savedInstanceState: Bundle?) {
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT

        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        val toolbar: Toolbar = binding.toolbar2
        setSupportActionBar(toolbar)
        val (navView: BottomNavigationView, navController) = setupNavController()
        setUpToolbar(navController, navView)
        val optionsHome = NavOptions.Builder()
            .setLaunchSingleTop(true)
            .setEnterAnim(R.anim.enter_from_bottom)
            .setExitAnim(R.anim.exit_to_bottom)
            .setPopEnterAnim(R.anim.enter_from_bottom_pop)
            .setPopExitAnim(R.anim.exit_to_bottom)
            .setPopUpTo(navController.graph.startDestinationId, false)
            .build()
        val optionsTop = NavOptions.Builder()
            .setLaunchSingleTop(true)
            .setEnterAnim(R.anim.enter_from_bottom_top)
            .setExitAnim(R.anim.exit_to_bottom)
            .setPopEnterAnim(R.anim.enter_from_bottom_pop)
            .setPopExitAnim(R.anim.exit_to_bottom)
            .setPopUpTo(navController.graph.startDestinationId, false)
            .build()
        binding.bottomNavView.setOnItemSelectedListener { item ->
            when (item.itemId) {
                R.id.navigation_top_ranked -> navController.navigate(
                    resId = R.id.navigation_top_ranked,
                    args = null,
                    navOptions = optionsTop
                )

                R.id.navigation_home -> navController.navigate(
                    resId = R.id.navigation_home,
                    args = null,
                    navOptions = optionsHome
                )


                R.id.navigation_profile -> navController.navigate(
                    resId = R.id.navigation_profile,
                    args = null,
                    navOptions = optionsHome
                )
            }
            true
        }
        window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
    } // End of onCreate


    private fun setupNavController(): Pair<BottomNavigationView, NavController> {
        val navView: BottomNavigationView = binding.bottomNavView

        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment_activity_main) as NavHostFragment
        val navController = navHostFragment.navController
        return Pair(navView, navController)
    }

    val fragmentsNavsIds =
        listOf(R.id.navigation_top_ranked, R.id.navigation_home, R.id.navigation_profile)

    private fun setUpToolbar(
        navController: NavController,
        navView: BottomNavigationView,
    ) {
        val appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.navigation_top_ranked, R.id.navigation_home, R.id.navigation_profile
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
        navController.addOnDestinationChangedListener { _, destination, _ ->
            if (fragmentsNavsIds.contains(destination.id))
                showBottomNavigationView()
            else hideBottomNavigationView()
        }
    }

    private fun hideBottomNavigationView() {
        binding.bottomNavView.animate().translationY(binding.bottomNavView.height.toFloat())
    }

    private fun showBottomNavigationView() {
        binding.bottomNavView.animate().translationY(0f)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}