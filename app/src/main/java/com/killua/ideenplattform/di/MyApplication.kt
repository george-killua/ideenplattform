package com.killua.ideenplattform.di

import android.app.Application
import android.content.Context
import android.net.*
import android.net.NetworkCapabilities.NET_CAPABILITY_INTERNET
import android.os.Build
import android.util.Log
import androidx.annotation.RequiresApi
import com.ashokvarma.gander.Gander
import com.ashokvarma.gander.persistence.GanderPersistence
import com.jakewharton.threetenabp.AndroidThreeTen
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MyApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@MyApplication)
            modules( httpModule,apiModule,moduleBuilder, repoModule)
        }
        instance = this
        Gander.setGanderStorage(GanderPersistence.getInstance(this))
        AndroidThreeTen.init(this)
    }

    companion object {
        lateinit var instance: MyApplication
            private set
    }
}