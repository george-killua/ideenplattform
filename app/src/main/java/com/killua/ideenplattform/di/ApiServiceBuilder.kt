package com.killua.ideenplattform.di

import com.killua.ideenplattform.data.network.ApiServices
import retrofit2.Retrofit

object ApiServiceBuilder{
    fun servicesApi(retrofit: Retrofit): ApiServices =
        retrofit.create(ApiServices::class.java)
}