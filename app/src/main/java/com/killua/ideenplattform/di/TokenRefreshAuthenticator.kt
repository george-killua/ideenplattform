package com.killua.ideenplattform.di

import android.util.Log
import com.killua.ideenplattform.data.authorizition.TokenUserDao
import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route

class TokenRefreshAuthenticator(
    private val tokenUserDao: TokenUserDao,
) : Authenticator {
    override fun authenticate(route: Route?, response: Response): Request? = when {
        response.retryCount > 2 -> null
        else -> response.createSignedRequest()
    }

    private fun Response.createSignedRequest(): Request? = try {
        val accessToken = tokenUserDao.getToken()?.credential ?: ""
        request.signWithToken(accessToken)
    } catch (error: Throwable) {
        Log.e("george", error.message.toString())
        null
    }
}