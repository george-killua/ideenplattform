package com.killua.ideenplattform.di

import com.killua.ideenplattform.authorization.login.LoginViewModel
import com.killua.ideenplattform.authorization.register.RegisterViewModel
import com.killua.ideenplattform.data.models.HttpClientHelper
import com.killua.ideenplattform.ideamain.details.DetailViewModel
import com.killua.ideenplattform.ideamain.editprofile.EditProfileViewModel
import com.killua.ideenplattform.ideamain.home.HomeViewModel
import com.killua.ideenplattform.ideamain.home.rating.RatingViewModel
import com.killua.ideenplattform.ideamain.newidee.ManagementIdeeViewModel
import com.killua.ideenplattform.ideamain.profile.ProfileViewModel
import org.koin.android.ext.koin.androidApplication
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import java.lang.ref.WeakReference

val httpModule =
    module {

        single { HttpBuilder.provideHttpClient(get(), get()) }
        single { HttpClientHelper(get()) }
    }
val apiModule by lazy {
    module {
        single {
            RetrofitBuilder.provideRetrofit(get())
        }

        single { ApiServiceBuilder.servicesApi(get()) }
    }
}
val repoModule = module {
    single { RepositoryBuilder.providerMainRepository(get(), androidContext(), get()) }
}
val moduleBuilder by lazy {
    module {
        single { DatabaseBuilder.db(androidApplication().applicationContext) }
        single { DatabaseBuilder.provideUserTokenDao(get()) }
        //viewModels
        viewModel { LoginViewModel(get()) }
        viewModel { RegisterViewModel(get()) }
        viewModel { HomeViewModel(get()) }
        viewModel { ManagementIdeeViewModel(get()) }
        viewModel { DetailViewModel(get()) }
        viewModel { ProfileViewModel(get()) }
        viewModel { list -> RatingViewModel(list.get()) }
        viewModel { EditProfileViewModel(get(), WeakReference(androidContext())) }
    }
}