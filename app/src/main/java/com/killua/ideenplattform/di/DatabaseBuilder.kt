package com.killua.ideenplattform.di

import android.content.Context
import androidx.room.Room
import com.killua.ideenplattform.data.authorizition.AuthDb
import com.killua.ideenplattform.data.authorizition.TokenUserDao


object DatabaseBuilder {
    fun db(applicationContext: Context) = Room.databaseBuilder(
        applicationContext,
        AuthDb::class.java, "database-name"
    ).allowMainThreadQueries().build()

    fun provideUserTokenDao(database: AuthDb): TokenUserDao {
        return database.tokenUserDao()
    }
}

