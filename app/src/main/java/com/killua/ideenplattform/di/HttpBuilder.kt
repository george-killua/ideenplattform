package com.killua.ideenplattform.di

import android.content.Context
import com.ashokvarma.gander.GanderInterceptor
import com.killua.ideenplattform.data.authorizition.TokenUserDao
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import java.util.concurrent.TimeUnit

object HttpBuilder {

    fun provideHttpClient(
        context: Context,
        tokenUserDao: TokenUserDao
        ): OkHttpClient {
        val logging = HttpLoggingInterceptor()
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC)
        val client = OkHttpClient.Builder()
        val authorizationInterceptor = AuthorizationInterceptor(tokenUserDao)
        val tokenRefreshAuthenticator = TokenRefreshAuthenticator(tokenUserDao)
        client.addInterceptor(logging)
        client.addInterceptor(authorizationInterceptor)
        client.authenticator(tokenRefreshAuthenticator)
        client.addInterceptor(
            GanderInterceptor(context).showNotification(true)
                .redactHeader("Cookie").retainDataFor(GanderInterceptor.Period.ONE_WEEK))
        client.connectTimeout(10, TimeUnit.SECONDS)
        client.writeTimeout(10, TimeUnit.SECONDS)
        client.readTimeout(30, TimeUnit.SECONDS)
        return client.build()
    }
}