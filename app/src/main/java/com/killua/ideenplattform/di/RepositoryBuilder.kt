package com.killua.ideenplattform.di

import android.content.Context
import com.killua.ideenplattform.data.authorizition.TokenUserDao
import com.killua.ideenplattform.data.network.ApiServices
import com.killua.ideenplattform.data.repository.MainRepository
import com.killua.ideenplattform.data.repository.MainRepositoryImpl

object RepositoryBuilder{
    fun providerMainRepository(
        api: ApiServices, context: Context,
        tokenUserDao: TokenUserDao,
    ): MainRepository = MainRepositoryImpl(api, context, tokenUserDao)
}