package com.killua.ideenplattform.di

import com.killua.ideenplattform.data.authorizition.TokenUserDao
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response

class AuthorizationInterceptor(
    private val tokenUserDao: TokenUserDao,
) : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val newRequest = chain.request().signedRequest()
        return chain.proceed(newRequest)
    }

    private fun Request.signedRequest(): Request {
        val accessToken = tokenUserDao.getToken()?.credential ?: ""
        return newBuilder()
            .header("Authorization", accessToken)
            .build()
    }
}

val Response.retryCount: Int
    get() {
        var currentResponse = priorResponse
        var result = 0
        while (currentResponse != null) {
            result++
            currentResponse = currentResponse.priorResponse
        }
        return result
    }

fun Request.signWithToken(accessToken: String) =
    newBuilder()
        .header("Authorization", accessToken)
        .build()

