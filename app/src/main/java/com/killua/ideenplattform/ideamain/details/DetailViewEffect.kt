package com.killua.ideenplattform.ideamain.details

import androidx.annotation.StringRes
import com.killua.ideenplattform.R
import com.killua.ideenplattform.data.models.User

sealed class DetailViewEffect {
    object NavToHome : DetailViewEffect()
    object RefreshLayout : DetailViewEffect()
    data class MakeToast(@StringRes val message: Int = R.string.internet_error) : DetailViewEffect()
    data class ShowUserDialog(val user: User) : DetailViewEffect()
    object ShowComments : DetailViewEffect()
    object CloseKeyboard : DetailViewEffect()
}