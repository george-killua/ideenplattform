package com.killua.ideenplattform.ideamain.newidee

import android.net.Uri
import android.util.Log.e
import com.killua.ideenplattform.R
import com.killua.ideenplattform.data.models.Idea
import com.killua.ideenplattform.data.repository.MainRepository
import com.killua.ideenplattform.data.requests.CreateIdeeReq
import com.killua.ideenplattform.ideamain.uiutils.BaseViewModel
import kotlinx.coroutines.flow.collect
import java.io.File
import java.util.*

class ManagementIdeeViewModel(private val userRepository: MainRepository) :
    BaseViewModel<ManagementStateUiDB, ManagementViewEffect,
            ManagementStateUi>(ManagementStateUi(), ManagementStateUiDB()) {
    private val lang: String = Locale.getDefault().displayLanguage
    var ideaId = ""
    private var imageOldPath: String = ""
    fun setIntent(action: ManagementAction) {
        when (action) {
            is ManagementAction.SetupMe -> setupMyFragment(action.ideaId)
            ManagementAction.SaveChanges -> saveChanges()
            ManagementAction.TextChanged -> textHasChanged()
            is ManagementAction.UploadImage -> uploadPic()
        }
    }

    private fun uploadPic() {
        reducerDB {
            if (imagePathUri == null)
                postEffect(ManagementViewEffect.TakePic)
            else {
                postEffect(ManagementViewEffect.MakeToast(R.string.delete_image))
                imagePathUri = null
            }
            copy(imagePathUri = imagePathUri)
        }
    }

    private fun saveChanges() {
        reducerDB {
            if (hasInternetConnection())
                launchCoroutine {
                    var categoryId = ""
                    userRepository.getAllCategories().collect { repoResult ->
                        try {
                            val categoryName: String = categoriesArray[selectedCategory - 1]
                            categoryId =
                                repoResult.data?.first { it.name_de == categoryName || it.name_en == categoryName }?.id!!
                        } catch (e: Exception) {
                            if (e.localizedMessage != null) e(
                                this.javaClass.simpleName,
                                e.localizedMessage!!
                            )
                        }
                    }
                    val createIdeeReq = ideaName?.let {
                        description?.let { it1 ->
                            CreateIdeeReq(
                                title = it.trim(),
                                categoryId = categoryId.trim(),
                                description = it1.trim()
                            )
                        }
                    }
                    if (ideaId.isBlank()) createIdeeReq?.let { ideeReq ->
                        userRepository.createNewIdea(
                            ideeReq,
                            File(Uri.parse(imagePathUri).path ?: "")
                        ).collect {
                            if (it.data != null)
                                postEffect(ManagementViewEffect.NavigateToDetailWithIdea(it.data))
                            else postEffect(ManagementViewEffect.MakeToast(R.string.internet_error))
                        }
                    }
                    else {

                        if (imageOldPath != imagePathUri && imagePathUri != null)
                            userRepository.uploadImageIdea(
                                ideaId,
                                File(Uri.parse(imagePathUri).path ?: "")
                            ).collect()
                        createIdeeReq?.let { ideeReq ->
                            userRepository.updateIdeaWithId(ideaId, ideeReq).collect {
                                if (it.data == true)
                                    postEffect(ManagementViewEffect.NavigateToDetailScreen)
                                else postEffect(ManagementViewEffect.MakeToast(R.string.internet_error))
                            }
                        }
                    }
                }
            else reducer { copy(internetConnectionError = true) }
            copy()
        }
    }

    private fun textHasChanged() {

        reducerDB {
            when {
                this.ideaName.isNullOrEmpty() -> {
                    reducer {
                        ManagementStateUi(titleISEmpty = R.string.idea_name_error)
                    }
                }
                this.description.isNullOrEmpty() -> {
                    reducer {
                        ManagementStateUi(descriptionIsEmpty = R.string.description_error)
                    }
                }
                selectedCategory < 1 -> {
                    reducer {
                        ManagementStateUi(categoryNotSelected = R.string.select_category)
                    }
                }
                imagePathUri.isNullOrEmpty() -> {
                    postEffect(ManagementViewEffect.MakeToast(R.string.select_image))
                }
                else -> reducer {
                    ManagementStateUi(isDataValid = true)
                }
            }
            copy()
        }
    }

    private fun setupMyFragment(ideaId: String?) {
        this.ideaId = ideaId ?: ""
        if (hasInternetConnection())
            launchCoroutine {

                userRepository.getAllCategories().collect { repoResultResult ->
                    repoResultResult.data?.let { categoriesList ->
                        reducerDB {
                            copy(categoriesArray = categoriesList.map {
                                if (lang == Locale.ENGLISH.language) it.name_en
                                else it.name_de
                            })
                        }
                    }
                }
                if (!ideaId.isNullOrBlank()) {
                    userRepository.getIdeaWithId(ideaId).collect {
                        val idea = it.data
                        if (idea is Idea) {
                            imageOldPath = idea.imageUrl
                            reducerDB {
                                copy(
                                    titleIsToEdit = true,
                                    ideaName = idea.title,
                                    imagePathUri = idea.imageUrl,
                                    description = idea.description,
                                    selectedCategory = (categoriesArray.indexOf(
                                        if (lang == Locale.ENGLISH.language) idea.category.name_en
                                        else idea.category.name_de
                                    )+1),
                                    btnSubmitTextIsToSave = true
                                )
                            }
                        }
                    }
                } else reducerDB { copy(titleIsToEdit = false) }
            }
        else reducer { copy(internetConnectionError = true) }
    }
}


