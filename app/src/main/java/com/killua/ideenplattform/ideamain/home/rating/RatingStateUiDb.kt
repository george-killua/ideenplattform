package com.killua.ideenplattform.ideamain.home.rating

import com.killua.ideenplattform.ideamain.home.rating.adapter.RatingAdapter

data class RatingStateUiDb(
    val adapter: RatingAdapter? = null,
    val all: String = "",
    val reallyCool: String = "",
    val cool: String = "",
    val okay: String = "",
    val bad: String = "",
    val reallyBad: String = ""
)