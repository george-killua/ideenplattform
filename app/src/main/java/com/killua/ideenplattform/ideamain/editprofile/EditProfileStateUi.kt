package com.killua.ideenplattform.ideamain.editprofile

data class EditProfileStateUi(
    val firstNameError: Int? = null,
    val lastNameError: Int? = null,
    val emailError: Int? = null,
    val passwordError: Int? = null,
    val passwordConfirmError: Int? = null,
    val isDataValid: Boolean = false,
    val networkError: Boolean = false,
    var isLoading: Boolean = false
)