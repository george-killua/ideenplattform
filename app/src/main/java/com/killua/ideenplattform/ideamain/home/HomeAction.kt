package com.killua.ideenplattform.ideamain.home

import com.killua.ideenplattform.ideamain.home.adapters.IdeaViewHolder

sealed class HomeAction {
    data class SetupFragment(val isTopRank: Boolean = false) : HomeAction()
    data class DetailsIdea(val ideaId: String) : HomeAction()
    data class LoadIdeasWithId(val ideaId: String) : HomeAction()
    data class RateIdea(val i: Int, val id: String, val viewHolder: IdeaViewHolder) : HomeAction()
    data class RemoveIdeaRate(val id: String, val viewHolder: IdeaViewHolder) : HomeAction()
    object AddIdea : HomeAction()
    data class SearchFilter(val query: String?) : HomeAction()
}