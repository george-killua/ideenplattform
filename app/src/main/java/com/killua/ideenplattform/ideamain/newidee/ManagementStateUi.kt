package com.killua.ideenplattform.ideamain.newidee

data class ManagementStateUi(
    val titleISEmpty: Int? = null,
    val descriptionIsEmpty: Int? = null,
    val categoryNotSelected: Int? = null,
    val isDataValid: Boolean = false,
    val internetConnectionError: Boolean = false
)