package com.killua.ideenplattform.ideamain.home

import com.killua.ideenplattform.ideamain.home.adapters.CategoryAdapter
import com.killua.ideenplattform.ideamain.home.adapters.IdeasAdapter
import com.killua.ideenplattform.ideamain.home.adapters.SortItemAdapter

data class HomeStateUiDb(
    val ideasAdapter: IdeasAdapter? = null,
    val categoriesAdapter: CategoryAdapter? = null,
    var sortTypeAdapter: SortItemAdapter? = null
)