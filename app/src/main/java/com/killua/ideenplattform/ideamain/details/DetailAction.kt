package com.killua.ideenplattform.ideamain.details

sealed class DetailAction {
    data class SetupFragment(val ideaId: String) : DetailAction()
    object UpdateRelease : DetailAction()
    object RemoveIdea : DetailAction()
    object CommentEmpty : DetailAction()
    object AddComment : DetailAction()
}