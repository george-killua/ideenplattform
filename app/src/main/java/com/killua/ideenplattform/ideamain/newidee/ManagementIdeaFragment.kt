package com.killua.ideenplattform.ideamain.newidee

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.TextView
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.github.drjacky.imagepicker.ImagePicker
import com.killua.ideenplattform.MainActivity
import com.killua.ideenplattform.R
import com.killua.ideenplattform.databinding.FragmentIdeaManagementBinding
import com.killua.ideenplattform.ideamain.uiutils.DataBindingAdapters.setCustomAdapter
import com.killua.ideenplattform.ideamain.uiutils.DataBindingAdapters.setImagePath
import com.killua.ideenplattform.ideamain.uiutils.safeNavigate
import com.killua.ideenplattform.ideamain.uiutils.showToast
import kotlinx.coroutines.flow.collect
import org.koin.androidx.viewmodel.ext.android.viewModel

class ManagementIdeaFragment : Fragment() {
    private val args: ManagementIdeaFragmentArgs by navArgs()
    private val viewModel by viewModel<ManagementIdeeViewModel>()
    private lateinit var binding: FragmentIdeaManagementBinding
    private lateinit var launcher: ActivityResultLauncher<Intent>
    private val obj = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }

        override fun afterTextChanged(s: Editable?) {
            viewModel.setIntent(ManagementAction.TextChanged)
        }

    }
    override fun onAttach(context: Context) {
        super.onAttach(context)
        launcher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { activityResult ->
            if (activityResult.resultCode == Activity.RESULT_OK) {
                activityResult.data?.let {
                    binding.ivIdeaImage.setImagePath(it.data.toString())
                    viewModel.reducerDB {
                        copy(imagePathUri = it.data!!.toString())
                    }
                    viewModel.setIntent(ManagementAction.TextChanged)

                }
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        binding = FragmentIdeaManagementBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val id = args.idOfIdea
        binding.ideaVm = viewModel
        binding.saveChanges = ManagementAction.SaveChanges
        binding.uploadImage = ManagementAction.UploadImage
        binding.etDescription.editText?.addTextChangedListener(obj)
        binding.etName.editText?.addTextChangedListener(obj)
        binding.btnAddIdea.text=if(id==null)getString(R.string.title_new_idea) else getString(R.string.title_edit_idea)
        setHasOptionsMenu(true)
        (activity as MainActivity?)?.supportActionBar?.title = if(id==null)getString(R.string.title_new_idea) else getString(R.string.title_edit_idea)

        viewModel.apply {
            //fragment setup ideamain
            setIntent(ManagementAction.SetupMe(id))
            //assigned effect, stateUi and stateUI data binding
            setupEffect(id)
            setupStateUiDb()
            setupStateUi()
            //end
            //setup ideamain changes and buttons
            setupButtons()
        }
    }

    private fun ManagementIdeeViewModel.setupStateUi() {
        lifecycleScope.launchWhenStarted {
            getStateUi.collect {
                (activity as MainActivity?)?.binding?.offLine?.visibility =
                    if (it.internetConnectionError) View.VISIBLE else View.GONE
                    setupUIState(it)
            }
        }
    }

    private fun setupUIState(state: ManagementStateUi) {
        with(binding) {
            btnAddIdea.isEnabled = state.isDataValid
            state.titleISEmpty.let { errorMessage ->
                if (errorMessage != null)
                    etName.error = getString(errorMessage)
                etName.isErrorEnabled = errorMessage != null
            }
            state.descriptionIsEmpty.let { errorMessage ->
                if (errorMessage != null)
                    etDescription.error = getString(errorMessage)
                etDescription.isErrorEnabled = errorMessage != null
            }

            state.categoryNotSelected.let { errorMessage ->
                if (errorMessage != null)
                    setCategorySpinnerError()
            }
        }
    }
    private fun ManagementIdeeViewModel.setupStateUiDb() {
        lifecycleScope.launchWhenCreated {
            getStateUiDb.collect {
                binding.ideaDb = it

                binding.spCategory.setCustomAdapter(it.categoriesArray.toMutableList())
                binding.spCategory.setSelection(it.selectedCategory)
                binding.executePendingBindings()
            }
        }
    }

    private fun ManagementIdeeViewModel.setupEffect(id: String?) {
        lifecycleScope.launchWhenCreated {
            getViewEffects.collect {
                when (it) {
                    is ManagementViewEffect.MakeToast ->
                        showToast(it.toastMessage)
                    ManagementViewEffect.NavigateToDetailScreen ->
                        navigateToDetail(id!!)
                    is ManagementViewEffect.NavigateToDetailWithIdea ->
                        navigateToDetail(it.idea.id)
                    ManagementViewEffect.TakePic -> selectImage()
                }
            }
        }
    }

    private fun ManagementIdeeViewModel.setupButtons() {
        binding.etName.editText?.doAfterTextChanged {
            viewModel.reducerDB {
                copy(ideaName = it.toString())
            }
        }

        binding.etDescription.editText?.doAfterTextChanged {
            viewModel.reducerDB {
                copy(description = it.toString())
            }
        }

        binding.spCategory.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long,
                ) {
                    reducerDB {
                        copy(selectedCategory = position)
                    }
                    viewModel.setIntent(ManagementAction.TextChanged)
                }

                override fun onNothingSelected(parent: AdapterView<*>?) {
                    reducerDB {
                        copy(selectedCategory = 0)
                    }
                    val errorTextview = binding.spCategory.selectedView as TextView
                    errorTextview.error = ""
                    errorTextview.setTextColor(Color.RED)
                    errorTextview.text = ""
                }
            }
    }





    private fun navigateToDetail(ideaId: String) {
        val action = ManagementIdeaFragmentDirections.managToDetail(ideaId)
        findNavController().safeNavigate(action)
    }



    private fun setCategorySpinnerError() {
        val selectedView: View = binding.spCategory.selectedView
        if (selectedView is TextView) {
            val errorString = getString(R.string.select_category)
            selectedView.error = errorString
        }
    }

    private fun selectImage() {
        ImagePicker.with(requireActivity())
            .cropSquare()
            .maxResultSize(1024, 1024)
            .createIntentFromDialog { launcher.launch(it) }
    }
}