package com.killua.ideenplattform.ideamain.home

data class HomeStateUi(
    val isLoadingProgressBar: Boolean = false,
    val internetConnectionError: Boolean = false
)