package com.killua.ideenplattform.ideamain.details.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.killua.ideenplattform.data.models.IdeaComment
import com.killua.ideenplattform.data.models.User
import com.killua.ideenplattform.databinding.ItemCommentBinding
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneOffset
import org.threeten.bp.format.DateTimeFormatter

class CommentsAdapter(
    val user: User,
    private val deleteFromCommentAdapter: DeleteFromCommentAdapter,
) :
    ListAdapter<IdeaComment, CommentsViewHolder>(object :
        DiffUtil.ItemCallback<IdeaComment>() {
        override fun areItemsTheSame(oldItem: IdeaComment, newItem: IdeaComment) =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: IdeaComment, newItem: IdeaComment) =
            oldItem == newItem

    }) {
    var view: View? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CommentsViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        view = parent.rootView
        return CommentsViewHolder(
            ItemCommentBinding.inflate(layoutInflater, parent, false),
            deleteFromCommentAdapter, user
        )
    }

    override fun submitList(list: MutableList<IdeaComment>?) {
        super.submitList(list?.sortedByDescending {
            val dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
            LocalDateTime.parse(it.created, dateFormat).toEpochSecond(ZoneOffset.UTC)
        })
    }

    override fun onBindViewHolder(holder: CommentsViewHolder, position: Int) {
        holder.bind(user, getItem(position))
    }
}