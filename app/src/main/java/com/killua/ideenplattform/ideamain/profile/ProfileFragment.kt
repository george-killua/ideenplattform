package com.killua.ideenplattform.ideamain.profile

import android.content.Intent
import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.killua.ideenplattform.MainActivity
import com.killua.ideenplattform.R
import com.killua.ideenplattform.authorization.AuthenticationActivity
import com.killua.ideenplattform.databinding.FragmentProfileBinding
import com.killua.ideenplattform.ideamain.uiutils.safeNavigate
import com.killua.ideenplattform.ideamain.uiutils.showToast
import kotlinx.coroutines.flow.collect
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProfileFragment : Fragment() {
    private val profileViewModel by viewModel<ProfileViewModel>()
    private var _binding: FragmentProfileBinding? = null
    private val binding get() = _binding!!
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {

        _binding = FragmentProfileBinding.inflate(inflater, container, false)
        return binding.root
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onStart() {
        super.onStart()
        lifecycleScope.launchWhenCreated {
            profileViewModel.getViewEffects.collect {
                onViewEffectReceived(it)
            }
        }

        lifecycleScope.launchWhenCreated {
            profileViewModel.getStateUi.collect { onState(it) }
        }
    }

    override fun onResume() {
        super.onResume()
        profileViewModel.setIntent(ProfileAction.SetupMyFragment)
        lifecycleScope.launchWhenResumed {
            profileViewModel.getStateUiDb.collect {
                onStateDataBinding(it)

            }
        }
    }

    private fun onViewEffectReceived(effect: ProfileViewEffect?) {
        when (effect) {
            ProfileViewEffect.NavigateToAddIdea -> {
                val action = ProfileFragmentDirections.profileToNewIdea()
                findNavController().safeNavigate(action)
            }
            ProfileViewEffect.NavigateToEditProfile -> {
                val action = ProfileFragmentDirections.profileToEditProfile()
                findNavController().safeNavigate(action)
            }
            ProfileViewEffect.NavigateToLoginFragment -> {
                activity?.let { authActivity ->
                    val navy = Intent(
                        authActivity,
                        AuthenticationActivity::class.java
                    )
                    authActivity.finishAffinity()
                    startActivity(navy)
                }
            }
            is ProfileViewEffect.MakeToast -> showToast(effect.message)
        }
    }

    private fun onStateDataBinding(stateViewDb: ProfileStateUiDb?) {
        binding.state = stateViewDb
        binding.executePendingBindings()

    }

    private fun onState(state: ProfileStateUi) {
        (activity as MainActivity?)?.binding?.offLine?.visibility =
            if (state.internetConnectionError) View.VISIBLE else View.GONE
        if (state.toastMessage != null) showToast(state.toastMessage)

        binding.progressBar.visibility =
        if (state.isLoading) View.VISIBLE else View.GONE
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.profile_menu, menu)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.pm_edit_profile -> profileViewModel.setIntent(ProfileAction.OnEditProfileAction)
            R.id.pm_new_idea -> profileViewModel.setIntent(ProfileAction.OnAddNewIdeaAction)
            R.id.pm_sign_out -> profileViewModel.setIntent(ProfileAction.OnSignOutAction)
        }
        return super.onOptionsItemSelected(item)
    }
}