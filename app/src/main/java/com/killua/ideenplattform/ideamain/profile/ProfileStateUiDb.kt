package com.killua.ideenplattform.ideamain.profile

data class ProfileStateUiDb(
    val imageProfileUrl: String? = null,
    val fullName: String = "",
    val email: String = "",
    val firstname: String = "",
    val lastname: String = "",
    val rol: Boolean = false
)