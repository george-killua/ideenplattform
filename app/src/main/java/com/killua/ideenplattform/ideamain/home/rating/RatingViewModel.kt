package com.killua.ideenplattform.ideamain.home.rating

import com.killua.ideenplattform.data.models.IdeaRating
import com.killua.ideenplattform.data.models.User
import com.killua.ideenplattform.ideamain.home.rating.adapter.RatingAdapter
import com.killua.ideenplattform.ideamain.uiutils.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking

class RatingViewModel(private val ratings: List<IdeaRating>) :
    BaseViewModel<RatingStateUiDb, RatingViewEffect, RatingStateUi>(RatingStateUi(),
        RatingStateUiDb()), EmojiClicked {
    init {
        setIntent(RatingAction.SetupMyRatingDialog)
    }
    fun setIntent(action: RatingAction) {
        when (action) {
            RatingAction.SetupMyRatingDialog -> {

                reducerDB {
                    val adapter = RatingAdapter(this@RatingViewModel)
                    adapter.submitList(ratings)
                    val allCount = ratings.count()
                    val reallyCoolCount = ratings.filter { it.rating == 5 }.count()
                    val coolCount = ratings.filter { it.rating == 4 }.count()
                    val okayCount = ratings.filter { it.rating == 3 }.count()
                    val badCount = ratings.filter { it.rating == 2 }.count()
                    val reallyBadCount = ratings.filter { it.rating == 1 }.count()

                    copy(
                        adapter = adapter,
                        all = allCount.toString(),
                        reallyCool = reallyCoolCount.toString(),
                        cool = coolCount.toString(),
                        okay = okayCount.toString(),
                        bad = badCount.toString(),
                        reallyBad = reallyBadCount.toString()
                    )


                }
            }
            RatingAction.AllClicked -> allClicked()
            RatingAction.ReallyBadClicked -> reallyBadClicked()
            RatingAction.BadClicked -> badClicked()
            RatingAction.CoolClicked -> coolClicked()
            RatingAction.OkayClicked -> okayClicked()
            RatingAction.ReallyCoolClicked -> reallyCoolClicked()
        }
    }

    private fun allClicked() {
        reducerDB {
            runBlocking(Dispatchers.Main) {
                adapter?.submitList(ratings)
                postEffect(RatingViewEffect.SmoothScrolling)
            }
            copy(adapter = adapter)
        }

    }

    private fun reallyCoolClicked() {
        reducerDB {
            runBlocking(Dispatchers.Main) {
                adapter?.submitList(ratings.filter { it.rating == 5 })
                postEffect(RatingViewEffect.SmoothScrolling)
            }
            copy(adapter = adapter)
        }

    }

    private fun coolClicked() {
        reducerDB {
            runBlocking(Dispatchers.Main) {
                adapter?.submitList(ratings.filter { it.rating == 4 })
                postEffect(RatingViewEffect.SmoothScrolling)
            }
            copy(adapter = adapter)
        }

    }

    private fun okayClicked() {
        reducerDB {
            runBlocking(Dispatchers.Main) {
                adapter?.submitList(ratings.filter { it.rating == 3 })
                postEffect(RatingViewEffect.SmoothScrolling)
            }
            copy(adapter = adapter)
        }

    }

    private fun badClicked() {
        reducerDB {
            runBlocking(Dispatchers.Main) {
                adapter?.submitList(ratings.filter { it.rating == 2 })
                postEffect(RatingViewEffect.SmoothScrolling)
            }
            copy(adapter = adapter)
        }

    }

    private fun reallyBadClicked() {
        reducerDB {
            runBlocking(Dispatchers.Main) {
                adapter?.submitList(ratings.filter { it.rating == 1 })
                postEffect(RatingViewEffect.SmoothScrolling)
            }
            copy(adapter = adapter)
        }

    }

    override fun userImageClicked(user: User) {
        postEffect(RatingViewEffect.ShowUserDialog(user))
    }
}