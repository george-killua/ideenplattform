package com.killua.ideenplattform.ideamain.home.rating

import androidx.annotation.StringRes
import com.killua.ideenplattform.R
import com.killua.ideenplattform.data.models.User

sealed class RatingViewEffect {
    data class ShowUserDialog(val user: User) : RatingViewEffect()
    object SmoothScrolling : RatingViewEffect()
    data class MakeToast(@StringRes val message: Int= R.string.internet_error) : RatingViewEffect()

}