package com.killua.ideenplattform.ideamain.details.adapter

import com.killua.ideenplattform.data.models.User

interface DeleteFromCommentAdapter {
    fun removeComment(commentId: String)
    fun dialogProfileShow(user: User)
}