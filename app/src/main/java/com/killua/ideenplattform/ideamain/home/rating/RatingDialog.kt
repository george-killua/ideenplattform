package com.killua.ideenplattform.ideamain.home.rating

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.lifecycleScope
import com.killua.ideenplattform.data.models.IdeaRating
import com.killua.ideenplattform.databinding.PopupLikedDialogBinding
import com.killua.ideenplattform.ideamain.uiutils.showUserProfileDialog
import kotlinx.coroutines.flow.collect
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

class RatingDialog(private val ratings: List<IdeaRating>) :
    DialogFragment() {
    private var _binding: PopupLikedDialogBinding? = null
    val viewModel: RatingViewModel by viewModel { parametersOf(ratings) }

    val binding get() = _binding!!

    companion object {
        @JvmStatic
        fun newInstance(ratings: List<IdeaRating>): RatingDialog {
            return RatingDialog(ratings)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = PopupLikedDialogBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        requireDialog().setCanceledOnTouchOutside(true)
        requireDialog().window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        val back = ColorDrawable(Color.TRANSPARENT)
        val inset = InsetDrawable(back, 35)
        requireDialog().window?.setBackgroundDrawable(inset)
        with(binding) {
            with(viewModel) {
                ivAll.setOnClickListener {
                    setIntent(RatingAction.AllClicked)
                }
                ivReallyCool.setOnClickListener {
                    setIntent(RatingAction.ReallyCoolClicked)
                }
                ivCool.setOnClickListener {
                    setIntent(RatingAction.CoolClicked)
                }
                ivOkay.setOnClickListener {
                    setIntent(RatingAction.OkayClicked)
                }
                ivBad.setOnClickListener {
                    setIntent(RatingAction.BadClicked)
                }
                ivReallyBad.setOnClickListener {
                    setIntent(RatingAction.ReallyBadClicked)
                }
                lifecycleScope.launchWhenStarted {
                    getStateUiDb.collect {
                        stateUiDb = it
                        executePendingBindings()
                    }
                }
                lifecycleScope.launchWhenStarted {
                    getViewEffects.collect {
                        setupEffect(it)
                    }
                }
            }
        }
    }

    private fun PopupLikedDialogBinding.setupEffect(it: RatingViewEffect) {
        when (it) {
            is RatingViewEffect.ShowUserDialog -> showUserProfileDialog(
                it.user
            )
            RatingViewEffect.SmoothScrolling -> rvRating.smoothScrollToPosition(
                0
            )
            is RatingViewEffect.MakeToast -> Toast.makeText(
                context,
                getString(it.message),
                Toast.LENGTH_SHORT
            ).show()
        }
    }
}