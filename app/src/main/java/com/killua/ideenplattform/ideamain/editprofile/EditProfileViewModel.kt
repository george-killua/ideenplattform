package com.killua.ideenplattform.ideamain.editprofile

import android.content.Context
import android.net.Uri
import com.killua.ideenplattform.R
import com.killua.ideenplattform.data.models.User
import com.killua.ideenplattform.data.repository.MainRepository
import com.killua.ideenplattform.data.requests.UserCreateReq
import com.killua.ideenplattform.ideamain.editprofile.EditProfileViewModel.*
import com.killua.ideenplattform.ideamain.uiutils.BaseViewModel
import kotlinx.coroutines.flow.collect
import java.io.File
import java.lang.ref.WeakReference

class EditProfileViewModel(
    private val mainRepository: MainRepository,
    private val context: WeakReference<Context>,
) :
    BaseViewModel<EditProfileStateUiDB, EditProfileViewEffect, EditProfileStateUi>(
        EditProfileStateUi(),
        EditProfileStateUiDB()
    ) {
    private var currentUser = User()

    init {
        reducer { copy(isLoading = true) }
        if (hasInternetConnection())
            launchCoroutine {
                mainRepository.getMe().collect { repoResult ->
                    repoResult.data?.let { user ->
                        reducerDB {
                            copy(
                                imageUri = user.profilePicture,
                                firstName = user.firstname,
                                lastname = user.lastname
                            )
                        }
                        currentUser = user
                    }
                }
            }
        else
            reducer { copy(networkError = true) }
        reducer { copy(isLoading = false) }

    }

    fun setIntent(action: EditProfileAction) {

        when (action) {
            EditProfileAction.RemoveImage -> removeImage()
            is EditProfileAction.InsertImage -> insertImage(action.pathUri)
            EditProfileAction.ChooseImageClicked -> postEffect(EditProfileViewEffect.ChooseImage)
            EditProfileAction.TextChanged -> textHasChanged()
            EditProfileAction.SaveChangesClicked -> saveClick()
        }
    }

    private fun textHasChanged() {
        reducerDB {
            if (!isNameValid(firstName.trim())) {
                reducer {
                    EditProfileStateUi(firstNameError = R.string.firstname_error)
                }
            } else
                if (!isNameValid(lastname.trim())) {
                    reducer {
                        EditProfileStateUi(lastNameError = R.string.lastname_error)
                    }
                } else

                    if (!isPasswordValid(password.trim())) {
                        reducer {
                            EditProfileStateUi(passwordError = R.string.password_error)
                        }
                    } else if (!isPasswordConfirmValid(password.trim(), passwordConfirm.trim())) {
                        reducer {
                            EditProfileStateUi(passwordConfirmError = R.string.password_confirm_error)
                        }
                    } else
                        reducer {
                            EditProfileStateUi(isDataValid = true)
                        }
            copy()
        }
    }

    private fun isNameValid(name: String): Boolean {
        return name.length > 2 && name.trim().matches(regex = Regex("[a-zA-zäöüÄÖÜß]+([ '-][a-zA-ZäöüÄÖÜß]+)*"))

    }

    private fun isPasswordConfirmValid(password: String, passwordConfirm: String): Boolean {
        return password.trim() == passwordConfirm.trim()
    }

    private fun isPasswordValid(password: String): Boolean {
        return password.trim().length > 5&& password.trim().matches(Regex("^(?=.*?[A-ZäöüÄÖÜß])(?=.*?[a-zäöüÄÖÜß])(?=.*?[0-9])(?=.*?[#?!@\$%^&*-]).{8,}\$"))
    }

    private fun insertImage(pathUri: String) {
        reducerDB { copy(imageUri = pathUri) }
        if (hasInternetConnection())
            launchCoroutine {
                val uri = Uri.parse(pathUri)
                mainRepository.uploadUserImage(File(uri.path ?: "")).collect { repoResult ->
                    if (repoResult.isNetworkingData) {
                        context.get()?.let {
                            postEffect(EditProfileViewEffect.MakeToast(R.string.updated))
                        }
                    } else
                        context.get()?.let {
                            postEffect(EditProfileViewEffect.MakeToast(R.string.error_updating_image))
                        }
                }
            }
        else
            reducer { copy(networkError = true) }
    }

    private fun removeImage() {
        if (hasInternetConnection())
            launchCoroutine {
                mainRepository.deleteImageOfUser().collect { repoResult ->
                    if (repoResult.isNetworkingData)
                        context.get()?.let {
                            postEffect(EditProfileViewEffect.MakeToast(R.string.deleted))
                        }
                    else context.get()?.let {
                        postEffect(EditProfileViewEffect.MakeToast(R.string.error_deleting_image))
                    }
                }
                reducerDB { copy(imageUri = "") }
            } else
            reducer { copy(networkError = true) }
    }


    private fun saveClick() {
        if (hasInternetConnection())
            reducerDB {
                reducer { copy(isLoading = true) }
                launchCoroutine {
                    mainRepository.updateUser(
                        UserCreateReq(
                            email = currentUser.email.trim(),
                            firstname = this@reducerDB.firstName.trim(),
                            lastname = this@reducerDB.lastname.trim(),
                            password = this@reducerDB.password.trim()
                        )
                    ).collect {
                        if (!it.isNetworkingData)
                            postEffect(EditProfileViewEffect.MakeToast())
                    }
                }
                postEffect(EditProfileViewEffect.NavigateToProfile)
                reducer { copy(isLoading = false) }
                copy()
            }
        else
            reducer { copy(networkError = true) }
    }
}