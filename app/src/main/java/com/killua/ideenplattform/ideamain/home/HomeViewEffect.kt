package com.killua.ideenplattform.ideamain.home

import androidx.annotation.StringRes
import androidx.navigation.NavDestinationDsl
import androidx.navigation.NavDirections
import com.killua.ideenplattform.R
import com.killua.ideenplattform.data.models.IdeaRating
import com.killua.ideenplattform.data.models.User

sealed class HomeViewEffect {
    data class NavigateTo(@NavDestinationDsl val action: NavDirections) : HomeViewEffect()
    data class MakeToast(@StringRes val message: Int= R.string.internet_error) : HomeViewEffect()
    data class ShowUserDialog(val user: User) : HomeViewEffect()
    data class ShowRatingDialog(val ratings: List<IdeaRating>) : HomeViewEffect()
    object SmoothScroll : HomeViewEffect()
}