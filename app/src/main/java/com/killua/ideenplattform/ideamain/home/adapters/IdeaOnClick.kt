package com.killua.ideenplattform.ideamain.home.adapters

import com.killua.ideenplattform.data.models.IdeaRating
import com.killua.ideenplattform.data.models.User

interface IdeaOnClick {
    fun clicked(ideaId: String)
    fun emojiOnClickInsert(i: Int, id: String, viewHolder: IdeaViewHolder)
    fun emojiOnClickRemove(id: String, viewHolder: IdeaViewHolder)
    fun dialogProfileShow(user: User)
    fun dialogRatingShow(ratings: List<IdeaRating>)
}