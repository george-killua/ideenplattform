package com.killua.ideenplattform.ideamain.details

import com.killua.ideenplattform.data.models.Idea
import com.killua.ideenplattform.ideamain.details.adapter.CommentsAdapter

data class DetailsStateUiDb(
    val title:String="",
    var imageUrl: String? = "",
    val fullName: String? = "",
    val userImage: String? = null,
    val categoryName: String = "",
    val createDate:String="",
    val lastUpdateDate:String="",
    val description:String="",
    val isMyIdea: Boolean = false,
    val idea: Idea? = null,
    val isManager: Boolean = false,
    val adapter: CommentsAdapter? = null,
    var commentInput: String = ""
)