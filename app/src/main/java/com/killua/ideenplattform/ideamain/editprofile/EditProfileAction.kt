package com.killua.ideenplattform.ideamain.editprofile

import com.killua.ideenplattform.ideamain.home.HomeAction

sealed class EditProfileAction {
    object SaveChangesClicked : EditProfileAction()
    object ChooseImageClicked : EditProfileAction()
    object RemoveImage : EditProfileAction()
    object TextChanged : EditProfileAction()
    data class InsertImage(val pathUri: String) : EditProfileAction()
}