package com.killua.ideenplattform.ideamain.newidee

import androidx.annotation.StringRes
import com.killua.ideenplattform.R
import com.killua.ideenplattform.data.models.Idea

sealed class ManagementViewEffect {
    data class MakeToast(@StringRes val toastMessage: Int= R.string.internet_error) : ManagementViewEffect()
    object NavigateToDetailScreen : ManagementViewEffect()
    data class NavigateToDetailWithIdea(val idea: Idea) : ManagementViewEffect()
    object TakePic:ManagementViewEffect()
}