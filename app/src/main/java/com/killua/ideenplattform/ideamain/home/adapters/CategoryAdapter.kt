package com.killua.ideenplattform.ideamain.home.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.killua.ideenplattform.data.models.Category
import com.killua.ideenplattform.databinding.ItemCategoryBinding
import java.util.*

class CategoryAdapter(private val categoryOnClick: CategoryOnClick) :
    ListAdapter<Category, CategoryViewHolder>(object :
        DiffUtil.ItemCallback<Category>() {
        override fun areItemsTheSame(oldItem: Category, newItem: Category): Boolean =
            oldItem.id == newItem.id

        override fun areContentsTheSame(
            oldItem: Category,
            newItem: Category,
        ): Boolean = oldItem == newItem

    }) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemCategoryBinding.inflate(layoutInflater, parent, false)
        return CategoryViewHolder(binding, categoryOnClick)
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        val item = getItem(position)

        val lang = Locale.getDefault().displayLanguage
        holder.bind(
            if (lang == Locale.ENGLISH.language) item.name_en
            else item.name_de, item.id
        )
    }
}