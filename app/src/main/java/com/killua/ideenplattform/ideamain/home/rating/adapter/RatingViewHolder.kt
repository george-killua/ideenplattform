package com.killua.ideenplattform.ideamain.home.rating.adapter

import androidx.recyclerview.widget.RecyclerView
import com.killua.ideenplattform.R
import com.killua.ideenplattform.data.models.IdeaRating
import com.killua.ideenplattform.databinding.ItemRateListBinding
import com.killua.ideenplattform.ideamain.home.rating.EmojiClicked

class RatingViewHolder(val binding: ItemRateListBinding, private val click: EmojiClicked) :
    RecyclerView.ViewHolder(binding.root) {
    private val emojiList = listOf(
        R.drawable.ic_angry_emoji,
        R.drawable.ic_embarrassed,
        R.drawable.ic_fun_giggles_emoji,
        R.drawable.ic_chuckle_emoji,
        R.drawable.ic_amour_emoji
    )
    val emojisNames = binding.root.resources.getStringArray(R.array.rating_data)
    fun bind(item: IdeaRating) {

            binding.rate = item
            binding.tvRatedNum.text = emojisNames[(item.rating - 1)]
            binding.imageRateEmoji.setImageResource(emojiList[item.rating - 1])
            binding.imageProfile.setOnClickListener { click.userImageClicked(item.user) }
            binding.tvFullName.setOnClickListener { click.userImageClicked(item.user) }
            binding.executePendingBindings()

    }
}

