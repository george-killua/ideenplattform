package com.killua.ideenplattform.ideamain.profile

import androidx.annotation.StringRes
import com.killua.ideenplattform.R

sealed class ProfileViewEffect {
    object NavigateToEditProfile : ProfileViewEffect()
    object NavigateToAddIdea : ProfileViewEffect()
    object NavigateToLoginFragment : ProfileViewEffect()
    data class MakeToast(@StringRes val message: Int= R.string.internet_error) : ProfileViewEffect()

}