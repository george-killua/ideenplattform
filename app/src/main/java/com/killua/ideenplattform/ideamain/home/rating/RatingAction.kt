package com.killua.ideenplattform.ideamain.home.rating

sealed class RatingAction {
    object SetupMyRatingDialog : RatingAction()
    object AllClicked : RatingAction()
    object ReallyCoolClicked : RatingAction()
    object CoolClicked : RatingAction()
    object OkayClicked : RatingAction()
    object BadClicked : RatingAction()
    object ReallyBadClicked : RatingAction()
}