package com.killua.ideenplattform.ideamain.newidee

data class ManagementStateUiDB(
    val titleIsToEdit: Boolean = false,
    var imagePathUri: String? = null,
    var ideaName: String? = "",
    var description: String? = "",
    val categoriesArray: List<String> = listOf(),
    var selectedCategory: Int = 0,
    val btnSubmitTextIsToSave: Boolean = false,
)