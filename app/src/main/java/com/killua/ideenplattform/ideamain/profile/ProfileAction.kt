package com.killua.ideenplattform.ideamain.profile

sealed class ProfileAction {
    object OnEditProfileAction : ProfileAction()
    object OnAddNewIdeaAction : ProfileAction()
    object OnSignOutAction : ProfileAction()
    object SetupMyFragment : ProfileAction()
}