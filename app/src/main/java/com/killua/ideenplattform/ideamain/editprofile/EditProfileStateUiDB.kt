package com.killua.ideenplattform.ideamain.editprofile

data class EditProfileStateUiDB(
    var imageUri: String? = null,
    var firstName: String = "",
    var lastname: String = "",
    var password: String = "",
    var passwordConfirm: String = ""
)