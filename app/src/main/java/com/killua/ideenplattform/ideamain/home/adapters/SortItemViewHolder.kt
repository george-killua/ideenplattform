package com.killua.ideenplattform.ideamain.home.adapters

import androidx.recyclerview.widget.RecyclerView
import com.killua.ideenplattform.databinding.ItemSortLayoutBinding
import com.killua.ideenplattform.ideamain.home.adapters.SortItemOnClick

class SortItemViewHolder(
    val binding: ItemSortLayoutBinding,
    private val sortedItemOnClick: SortItemOnClick,
) :
    RecyclerView.ViewHolder(binding.root) {


    fun bind(name: String) {

        binding.root.setOnClickListener { sortedItemOnClick.itemClicked(name) }
        binding.tvSortName.text = name
    }
}