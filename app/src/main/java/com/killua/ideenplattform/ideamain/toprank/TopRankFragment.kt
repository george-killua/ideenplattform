package com.killua.ideenplattform.ideamain.toprank

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.killua.ideenplattform.MainActivity
import com.killua.ideenplattform.databinding.FragmentTopRankBinding
import com.killua.ideenplattform.ideamain.home.HomeAction
import com.killua.ideenplattform.ideamain.home.HomeViewEffect
import com.killua.ideenplattform.ideamain.home.HomeViewModel
import com.killua.ideenplattform.ideamain.uiutils.*
import kotlinx.coroutines.flow.collect
import org.koin.androidx.viewmodel.ext.android.viewModel

class TopRankFragment : Fragment() {
    private val viewModel by viewModel<HomeViewModel>()
    private var _binding: FragmentTopRankBinding? = null


    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentTopRankBinding.inflate(inflater, container, false)
        binding.refreshLayout.setOnRefreshListener {
            viewModel.setIntent(HomeAction.SetupFragment(false))
        }
        return binding.root

    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        viewModel.setIntent(HomeAction.SetupFragment(true))
        with(viewModel) {
            lifecycleScope.launchWhenStarted {

                getViewEffects.collect {
                    topEffect(it)
                }
            }
            lifecycleScope.launchWhenStarted {
                getStateUi.collect { state ->
                    binding.refreshLayout.isRefreshing = state.isLoadingProgressBar
                    (activity as MainActivity?)?.binding?.offLine?.visibility =
                        if (state.internetConnectionError) View.VISIBLE else View.GONE
                }
            }
            lifecycleScope.launchWhenCreated {
                getStateUiDb.collect {
                    binding.topRankMv = it
                    binding.executePendingBindings()

                }
            }
        }
    }

    private fun topEffect(it: HomeViewEffect) {
        when (it) {
            is HomeViewEffect.NavigateTo -> findNavController().safeNavigate(it.action)
            is HomeViewEffect.MakeToast -> showToast(it.message)
            HomeViewEffect.SmoothScroll -> binding.recyclerView.scrollToPosition(0)
            is HomeViewEffect.ShowUserDialog -> showUserProfileDialog(it.user)
            is HomeViewEffect.ShowRatingDialog -> showRatingDialog(it.ratings)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}