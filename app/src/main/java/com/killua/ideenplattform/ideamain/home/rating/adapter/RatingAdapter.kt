package com.killua.ideenplattform.ideamain.home.rating.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.killua.ideenplattform.data.models.IdeaRating
import com.killua.ideenplattform.databinding.ItemRateListBinding
import com.killua.ideenplattform.ideamain.home.rating.EmojiClicked

class RatingAdapter(private val click: EmojiClicked) :
    ListAdapter<IdeaRating, RatingViewHolder>(object : DiffUtil.ItemCallback<IdeaRating>() {
        override fun areItemsTheSame(oldItem: IdeaRating, newItem: IdeaRating) =
            oldItem == newItem

        override fun areContentsTheSame(oldItem: IdeaRating, newItem: IdeaRating) =
            oldItem.user == newItem.user
    }) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RatingViewHolder {
        val binding =
            ItemRateListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return RatingViewHolder(binding, click)
    }

    override fun onBindViewHolder(holder: RatingViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}
