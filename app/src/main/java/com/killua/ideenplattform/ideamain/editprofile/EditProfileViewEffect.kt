package com.killua.ideenplattform.ideamain.editprofile

import androidx.annotation.StringRes
import com.killua.ideenplattform.R

sealed class EditProfileViewEffect {
    object NavigateToProfile : EditProfileViewEffect()
    data class MakeToast(@StringRes val toastMessage: Int= R.string.internet_error) : EditProfileViewEffect()
    object ChooseImage : EditProfileViewEffect()
}