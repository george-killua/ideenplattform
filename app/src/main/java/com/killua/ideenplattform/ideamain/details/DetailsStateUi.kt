package com.killua.ideenplattform.ideamain.details

data class DetailsStateUi(
    val commentEmptyError: Boolean = false,
    val ShowComments: Boolean = false,
    val isLoadingProgressBar: Boolean = false,
    val networkError: Boolean = false
)