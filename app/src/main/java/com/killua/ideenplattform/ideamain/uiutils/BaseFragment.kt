package com.killua.ideenplattform.ideamain.uiutils

import android.app.AlertDialog
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.InsetDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.killua.ideenplattform.data.models.IdeaRating
import com.killua.ideenplattform.data.models.User
import com.killua.ideenplattform.databinding.PopupProfileBinding
import com.killua.ideenplattform.ideamain.home.rating.RatingDialog


    fun Fragment.showToast(@StringRes errorString: Int)
    =Toast.makeText(context, errorString, Toast.LENGTH_LONG).show()

    fun Fragment.showUserProfileDialog(user: User) : Dialog {
        val dialog = object : Dialog(requireContext()) {
            val binding by lazy { PopupProfileBinding.inflate(LayoutInflater.from(context)) }
             override fun onCreate(savedInstanceState: Bundle?) {
                setContentView(binding.root)
                binding.user = user
                binding.executePendingBindings()
                setCancelable(true)
                setCanceledOnTouchOutside(true)
                window?.setLayout(MATCH_PARENT, WRAP_CONTENT)
                val back = ColorDrawable(Color.TRANSPARENT)
                val inset = InsetDrawable(back, 35)
                window?.setBackgroundDrawable(inset)
            }
        }
        dialog.create()
        return dialog
    }

    fun Fragment.showRatingDialog(ratings: List<IdeaRating>) {
        val fm: FragmentManager = requireActivity().supportFragmentManager
        val ratingDialog: RatingDialog = RatingDialog.newInstance(ratings)

        ratingDialog.show(fm, "fragment_edit_name")
    }

     fun Fragment.showDialogYesNo(
        title: String,
        yesMessage: String,
        noMessage: String,
        yesPressed: () -> Unit,
    ) {
        val alertDialog = AlertDialog.Builder(context)
        alertDialog.setTitle(title)
        alertDialog.setPositiveButton(
            yesMessage
        ) { dialog, _ ->
            yesPressed()
            dialog.dismiss()
        }
        alertDialog.setNegativeButton(noMessage) { dialog, _ ->
            dialog.dismiss()
        }
        alertDialog.setCancelable(true)
        alertDialog.create().show()
    }

