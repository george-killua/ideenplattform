package com.killua.ideenplattform.ideamain.editprofile

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.github.drjacky.imagepicker.ImagePicker
import com.killua.ideenplattform.MainActivity
import com.killua.ideenplattform.R
import com.killua.ideenplattform.databinding.FragmentEditProfileBinding
import com.killua.ideenplattform.ideamain.uiutils.safeNavigate
import com.killua.ideenplattform.ideamain.uiutils.showDialogYesNo
import com.killua.ideenplattform.ideamain.uiutils.showToast
import kotlinx.coroutines.flow.collect
import org.koin.androidx.viewmodel.ext.android.viewModel

class EditProfileFragment : Fragment() {
    private val viewModel by viewModel<EditProfileViewModel>()
    private lateinit var binding: FragmentEditProfileBinding
    private lateinit var launcher: ActivityResultLauncher<Intent>
    private val obj = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }
        override fun afterTextChanged(s: Editable?) {
            viewModel.setIntent(EditProfileAction.TextChanged)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        launcher =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { activityResult ->
                if (activityResult.resultCode == Activity.RESULT_OK) {
                    activityResult.data?.let {
                        viewModel.setIntent(
                            EditProfileAction.InsertImage(it.data.toString())
                        )
                    }
                }
            }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        setHasOptionsMenu(true)
        binding = FragmentEditProfileBinding.inflate(inflater, container, false)
        binding.saveChanges = EditProfileAction.SaveChangesClicked
        binding.  chooseImage = EditProfileAction.ChooseImageClicked
        binding. viewModel = viewModel
        binding. executePendingBindings()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        with(binding) {

            etFirstName.editText?.addTextChangedListener(obj)
            etLastName.editText?.addTextChangedListener(obj)
            etPassword.editText?.addTextChangedListener(obj)
            etPasswordConfirm.editText?.addTextChangedListener(obj)
        }
        Toast.makeText(context, getString(R.string.editprofile_note), Toast.LENGTH_LONG).show()
    }

    override fun onResume() {
        super.onResume()
        with(viewModel) {
            lifecycleScope.launchWhenStarted {
                getStateUiDb.collect {
                    profileButtonHandler(it.imageUri.isNullOrBlank())
                    binding.stateDb = getStateUiDb.value
                    binding.executePendingBindings()
                }
            }
            lifecycleScope.launchWhenCreated {
                getStateUi.collect {
                    setupUIState(it)
                }
            }
            lifecycleScope.launchWhenStarted {
                viewModel.getViewEffects.collect {
                    effectHandler(it)
                }
            }

        }
    }

    private fun effectHandler(it: EditProfileViewEffect) {
        when (it) {
            EditProfileViewEffect.ChooseImage -> selectImage()
            is EditProfileViewEffect.MakeToast -> showToast(it.toastMessage)
            EditProfileViewEffect.NavigateToProfile -> navigateToProfile()
        }
    }

    private fun setupUIState(state: EditProfileStateUi) {

        with(binding) {
            btnSave.isEnabled = state.isDataValid
            state.firstNameError.let { errorMessage ->
                if (errorMessage != null)
                    etFirstName.error = getString(errorMessage)
                etFirstName.isErrorEnabled = errorMessage != null
            }
            state.lastNameError.let { errorMessage ->
                if (errorMessage != null)
                    etLastName.error = getString(errorMessage)
                etLastName.isErrorEnabled = errorMessage != null
            }

            state.passwordError.let { errorMessage ->
                if (errorMessage != null)
                    etPassword.error = getString(errorMessage)
                etPassword.isErrorEnabled = errorMessage != null
            }
            state.passwordConfirmError.let { errorMessage ->
                if (errorMessage != null)
                    etPasswordConfirm.error = getString(errorMessage)
                etPasswordConfirm.isErrorEnabled = errorMessage != null
            }
            (activity as MainActivity?)?.binding?.offLine?.visibility =
                if (state.networkError) View.VISIBLE else View.GONE
           contentLoadingProgressBar.visibility =
                if (state.isLoading) View.VISIBLE else View.GONE
        }
    }

    private fun profileButtonHandler(nullOrBlank: Boolean?) {
        if (nullOrBlank == true || nullOrBlank == null)
            binding.btnImageChooser.background = getDrawable(requireContext(), R.drawable.ic_add)
        else
            binding.btnImageChooser.background = getDrawable(requireContext(), R.drawable.ic_remove)
    }

    private fun navigateToProfile() {
        val action = EditProfileFragmentDirections.editProfileToProfile()
        findNavController().safeNavigate(action)
    }

    private fun selectImage() {

        if (viewModel.getStateUiDb.value.imageUri.isNullOrBlank())
            ImagePicker.with(requireActivity())
                .setImageProviderInterceptor { imageProvider -> //Intercept ImageProvider
                    Log.d("ImagePicker", "Selected ImageProvider: $imageProvider.name")
                }
                .cropSquare()
                .maxResultSize(1024, 1024)
                .createIntentFromDialog { launcher.launch(it) }
        else {
            val title = getString(R.string.delete_image)
            val yesMessage = getString(R.string.yes)
            val noMessage = getString(R.string.no)
            showDialogYesNo(
                title = title,
                yesMessage = yesMessage,
                noMessage = noMessage
            ) {
                viewModel.setIntent(EditProfileAction.RemoveImage)
            }
        }
    }


}


