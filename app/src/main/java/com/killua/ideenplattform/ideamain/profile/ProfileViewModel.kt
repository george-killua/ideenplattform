package com.killua.ideenplattform.ideamain.profile

import com.killua.ideenplattform.data.repository.MainRepository
import com.killua.ideenplattform.ideamain.uiutils.BaseViewModel
import kotlinx.coroutines.flow.collect

class ProfileViewModel(val repository: MainRepository) :
    BaseViewModel<ProfileStateUiDb, ProfileViewEffect, ProfileStateUi>(
        ProfileStateUi(),
        ProfileStateUiDb()
    ) {

    fun setIntent(intent: ProfileAction) {
        when (intent) {
            ProfileAction.OnAddNewIdeaAction -> navigateToAddIdea()
            ProfileAction.OnEditProfileAction -> navigateToEditProfile()
            ProfileAction.OnSignOutAction -> navigateToLoginFragment()
            ProfileAction.SetupMyFragment -> setUpFragment()
        }
    }

    private fun setUpFragment() {

        if (hasInternetConnection())
            launchCoroutine {
            reducer {
                copy(isLoading = true)
            }
           runCatching {
               repository.getMe().collect {
                   val userCaching = it.data
                   if (userCaching != null) {
                       reducerDB {
                           copy(
                               imageProfileUrl = userCaching.profilePicture,
                               fullName = userCaching.fullName,
                               email = userCaching.email,
                               firstname = userCaching.firstname,
                               lastname = userCaching.lastname,
                               rol = userCaching.isManager
                           )
                       }
                   }
               }

               reducer {
                   copy(isLoading = false)
               }
           }
        }
        else reducer { copy(internetConnectionError = true) }
    }


    private fun navigateToLoginFragment() {
        repository.deleteUserFomCaching()
        postEffect(ProfileViewEffect.NavigateToLoginFragment)
    }

    private fun navigateToAddIdea() {
        postEffect(ProfileViewEffect.NavigateToAddIdea)
    }

    private fun navigateToEditProfile() {
        postEffect(ProfileViewEffect.NavigateToEditProfile)
    }
}