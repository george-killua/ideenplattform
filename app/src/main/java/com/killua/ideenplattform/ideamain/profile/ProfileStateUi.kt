package com.killua.ideenplattform.ideamain.profile

import androidx.annotation.StringRes

data class ProfileStateUi(
    @StringRes val toastMessage: Int? = null,
    val isLoading: Boolean = false,
    val internetConnectionError: Boolean = false
)