package com.killua.ideenplattform.ideamain.details.adapter

import android.widget.PopupMenu
import androidx.recyclerview.widget.RecyclerView
import com.killua.ideenplattform.R
import com.killua.ideenplattform.data.models.IdeaComment
import com.killua.ideenplattform.data.models.User
import com.killua.ideenplattform.databinding.ItemCommentBinding


class CommentsViewHolder(
    val binding: ItemCommentBinding,
    private val deleteFromCommentAdapter: DeleteFromCommentAdapter,
    private val currentUser: User,
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(currentUser: User, item: IdeaComment) {
        binding.comment = item
        binding.user = currentUser
        binding.commentCreated.text = item.created.removeRange(10..item.created.lastIndex)
        binding.option.setOnClickListener { view ->
            val popup = PopupMenu(view.context, view)
            popup.inflate(R.menu.comment_menu)
            popup.setOnMenuItemClickListener {
                if (it.itemId == R.id.delete_comment) {

                    deleteFromCommentAdapter.removeComment(item.id)
                    return@setOnMenuItemClickListener true
                }
                return@setOnMenuItemClickListener false
            }
            popup.show()
        }
        setupButtons(item)
        binding.executePendingBindings()
    }

    private fun setupButtons(item: IdeaComment) {

        binding.commentProfileImage.setOnClickListener {
            if (item.user != currentUser) deleteFromCommentAdapter.dialogProfileShow(item.user)
        }
        binding.tvFullName.setOnClickListener {
            if (item.user != currentUser) deleteFromCommentAdapter.dialogProfileShow(item.user)
        }

    }
}

