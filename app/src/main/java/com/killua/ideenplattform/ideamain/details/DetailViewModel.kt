package com.killua.ideenplattform.ideamain.details

import android.content.Context
import com.killua.ideenplattform.R
import com.killua.ideenplattform.data.models.Idea
import com.killua.ideenplattform.data.models.User
import com.killua.ideenplattform.data.repository.MainRepository
import com.killua.ideenplattform.data.repository.RepoResult
import com.killua.ideenplattform.data.requests.CreateCommentReq
import com.killua.ideenplattform.data.requests.IdeaReleaseReq
import com.killua.ideenplattform.di.MyApplication
import com.killua.ideenplattform.ideamain.details.adapter.CommentsAdapter
import com.killua.ideenplattform.ideamain.details.adapter.DeleteFromCommentAdapter
import com.killua.ideenplattform.ideamain.uiutils.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.withContext
import java.util.*

class DetailViewModel(
    private val mainRepository: MainRepository,

) : BaseViewModel<DetailsStateUiDb, DetailViewEffect, DetailsStateUi>(
    DetailsStateUi(),
    DetailsStateUiDb()
), DeleteFromCommentAdapter {
    private val context: Context
    get() = MyApplication.instance.applicationContext

    private val lang = Locale.getDefault().displayLanguage
    private var ideaId = ""
    private var idea: Idea? = null
    private var isManager: Boolean = false
    private val currentUser: User by lazy {
        var temp = User()
        runBlocking {
            mainRepository.getMe().collect { repoResult -> repoResult.data?.let { temp = it } }
        }
        temp
    }

    init {
        reducerDB { copy(adapter = CommentsAdapter(currentUser, this@DetailViewModel)) }
    }

    fun setIntent(action: DetailAction) {

        when (action) {
            is DetailAction.SetupFragment -> {
                reducer { copy(isLoadingProgressBar = true) }
                ideaId = action.ideaId
                setupDataBinding()
            }
            is DetailAction.UpdateRelease -> if (isManager) updateRelease()
            DetailAction.CommentEmpty -> reducer { copy(commentEmptyError = true) }
            is DetailAction.AddComment -> reducerDB {
                postComment(commentInput)
                copy()
            }
            DetailAction.RemoveIdea -> removeIdea()
        }
    }

    private fun removeIdea() {
        if (hasInternetConnection())
            launchCoroutine {
                mainRepository.deleteIdeaWithId(idea!!.id)
                    .collect { repoResultResult ->
                        repoResultResult.data?.let { success ->
                            if (success) postEffect(DetailViewEffect.NavToHome)
                            else postEffect(DetailViewEffect.MakeToast(R.string.error_removing_idea))
                        }
                    }
            }
        else
            reducer { copy(networkError = true) }
    }

    private fun updateRelease() {
        if (hasInternetConnection())
            launchCoroutine {
                mainRepository.releaseIdea(idea!!.id, IdeaReleaseReq(!idea!!.released))
                    .collect { repoResultResult ->
                        repoResultResult.data?.let { success ->
                            if (success) postEffect(DetailViewEffect.NavToHome)
                            else postEffect(DetailViewEffect.MakeToast(R.string.error_update_release))
                        }
                    }
            } else
            reducer { copy(networkError = true) }
    }

    private fun setupDataBinding() {
        if (hasInternetConnection())
            launchCoroutine {
                mainRepository.getIdeaWithId(ideaId).collect {
                    idea = it.data
                    when (val ideaType = it.data) {
                        is Idea ->
                            reducerDB {
                                postEffect(DetailViewEffect.ShowComments)
                                runBlocking(Dispatchers.Main) {
                                    adapter!!.submitList(ideaType.comments.toMutableList())
                                    copy(
                                        idea = ideaType,
                                        title = ideaType.title,
                                        imageUrl = ideaType.imageUrl,
                                        fullName = ideaType.author.fullName,
                                        userImage = ideaType.author.profilePicture,
                                        categoryName = if (lang == Locale.ENGLISH.language)
                                            context.getString(R.string.category) + ideaType.category.name_en
                                        else
                                            context.getString(R.string.category) + ideaType.category.name_de,
                                        createDate = ideaType.created.removeRange(
                                            10,
                                            ideaType.created.lastIndex
                                        ),
                                        description = ideaType.description,
                                        adapter = adapter,
                                        lastUpdateDate = ideaType.lastUpdated.removeRange(
                                            10,
                                            ideaType.created.lastIndex
                                        ),
                                        isManager = currentUser.isManager,
                                        isMyIdea = currentUser.userId == ideaType.author.userId
                                    )

                                }
                            }

                        else -> postEffect(DetailViewEffect.NavToHome)
                    }
                    reducer { copy(isLoadingProgressBar = false) }
                }
            } else
            reducer { copy(networkError = true) }
    }

    private fun postComment(message: String) {
        if (hasInternetConnection())
            launchCoroutine {
                mainRepository.createComment(
                    ideaId = ideaId, createCommentReq = CreateCommentReq(message.trim())
                ).collect { repoResult: RepoResult<Boolean> ->
                    if (repoResult.data == true)
                        mainRepository.getComments(ideaId).collect {
                            if (it.isNetworkingData)
                                withContext(Dispatchers.Main) {
                                    reducerDB {
                                        adapter?.submitList(it.data?.toMutableList())
                                        copy(adapter = adapter, commentInput = "")
                                    }
                                }
                            postEffect(DetailViewEffect.CloseKeyboard)
                        }
                    else
                        postEffect(DetailViewEffect.MakeToast(R.string.error_posting_comment))
                }
            } else
            reducer { copy(networkError = true) }
    }


    override fun removeComment(commentId: String) {
        if (hasInternetConnection())
            launchCoroutine {
                mainRepository.deleteComments(ideaId, commentId).collect { repoResult ->
                    if (repoResult.data == true)
                        mainRepository.getComments(ideaId).collect {
                            if (it.isNetworkingData)
                                withContext(Dispatchers.Main) {
                                    reducerDB {
                                        adapter?.submitList(it.data?.toMutableList())
                                        copy(adapter = adapter)
                                    }
                                }
                        } else
                        postEffect(DetailViewEffect.MakeToast(R.string.error_removing_comment))
                }
            } else
            reducer { copy(networkError = true) }
    }

    override fun dialogProfileShow(user: User) {
        postEffect(DetailViewEffect.ShowUserDialog(user))
    }

}

