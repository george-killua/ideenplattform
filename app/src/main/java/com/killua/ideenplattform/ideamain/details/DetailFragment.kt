package com.killua.ideenplattform.ideamain.details

import android.os.Bundle
import android.view.*
import android.view.View.GONE
import android.view.View.VISIBLE
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.killua.ideenplattform.MainActivity
import com.killua.ideenplattform.R
import com.killua.ideenplattform.data.models.Idea
import com.killua.ideenplattform.databinding.FragmentDetailBinding
import com.killua.ideenplattform.ideamain.uiutils.*
import kotlinx.coroutines.flow.collect
import org.koin.androidx.viewmodel.ext.android.viewModel

class DetailFragment : Fragment() {
    private val detailViewModel by viewModel<DetailViewModel>()
    private var _binding: FragmentDetailBinding? = null
    private val binding get() = _binding!!
    private val args: DetailFragmentArgs by navArgs()
    private var isManager = false
    private var isMyIdea = false
    private var ideaId = ""
    private var menuList: Menu? = null
    private var idea: Idea? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentDetailBinding.inflate(inflater, container, false)
        ideaId = args.idOfDetails
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        with(binding) {
            detailVM = detailViewModel
            addComment = DetailAction.AddComment
            lifecycleScope.launchWhenStarted {
                detailViewModel.getStateUi.collect {
                    stateUiHandler(it)
                }
            }
            lifecycleScope.launchWhenResumed {
                detailViewModel.getViewEffects.collect {
                    effectHandler(it, view)
                }
            }
        }
    }

    private fun FragmentDetailBinding.effectHandler(
        it: DetailViewEffect,
        view: View
    ) {
        when (it) {
            is DetailViewEffect.MakeToast -> showToast(it.message)
            is DetailViewEffect.NavToHome -> navigateToHome()
            DetailViewEffect.RefreshLayout -> detailViewModel.setIntent(
                DetailAction.SetupFragment(
                    ideaId
                )
            )
            DetailViewEffect.ShowComments -> recyclerView.visibility = VISIBLE
            is DetailViewEffect.ShowUserDialog -> showUserProfileDialog(it.user)
            DetailViewEffect.CloseKeyboard -> view.hideKeyboard()
        }
    }

    private fun FragmentDetailBinding.stateUiHandler(it: DetailsStateUi) {
        if (it.ShowComments) recyclerView.visibility =
            VISIBLE else recyclerView.visibility = GONE
        etComment.isErrorEnabled = it.commentEmptyError
        if (it.commentEmptyError) {
            etComment.error = getString(R.string.comment_error)
        }
        progressBar.visibility =
            if (it.isLoadingProgressBar) VISIBLE else GONE
        (activity as MainActivity?)?.binding?.offLine?.visibility =
            if (it.networkError) VISIBLE else GONE
    }

    private fun navigateToHome() {
        val action = DetailFragmentDirections.detailsToHome(false)
        findNavController().safeNavigate(action)
    }

    override fun onResume() {
        super.onResume()
        detailViewModel.setIntent(DetailAction.SetupFragment(ideaId))
        lifecycleScope.launchWhenResumed {
            detailViewModel.getStateUiDb.collect {
                binding.detailDb = it
                isManager = it.isManager
                isMyIdea = it.isMyIdea
                idea = it.idea
                hideMenuItem(it)
                binding.executePendingBindings()
            }
        }
    }

    private fun hideMenuItem(it: DetailsStateUiDb) {
        val release = menuList?.findItem(R.id.release_idea)
        val remove = menuList?.findItem(R.id.remove_idea)
        val edit = menuList?.findItem(R.id.edit_idea)
        if (idea?.released == true) {
            release?.isVisible = false
            remove?.isVisible = false
            edit?.isVisible = false
            return
        }

        release?.isVisible = it.isManager
        remove?.isVisible = it.isMyIdea
        edit?.isVisible = it.isMyIdea
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        menuList = menu
        inflater.inflate(R.menu.idea_details_menu, menu)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.remove_idea -> showDialogYesNo(
                getString(R.string.delete_idea_title),
                getString(R.string.yes), getString(R.string.no)
            ) {
                detailViewModel.setIntent(DetailAction.RemoveIdea)
            }
            R.id.release_idea -> showDialogYesNo(
                getString(R.string.release_idea),
                getString(R.string.yes), getString(R.string.no)
            ) {
                detailViewModel.setIntent(DetailAction.UpdateRelease)
            }
            R.id.edit_idea -> findNavController().navigate(
                DetailFragmentDirections.detailToEdit(
                    ideaId
                )
            )
        }
        return super.onOptionsItemSelected(item)
    }
}


