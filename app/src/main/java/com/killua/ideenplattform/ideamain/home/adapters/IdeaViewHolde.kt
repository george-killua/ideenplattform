package com.killua.ideenplattform.ideamain.home.adapters

import android.annotation.SuppressLint
import android.graphics.Color
import android.util.Log
import android.util.TypedValue
import android.util.TypedValue.COMPLEX_UNIT_DIP
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.recyclerview.widget.RecyclerView
import com.github.pgreze.reactions.PopupGravity
import com.github.pgreze.reactions.ReactionSelectedListener
import com.github.pgreze.reactions.dsl.reactionPopup
import com.github.pgreze.reactions.dsl.reactions
import com.killua.ideenplattform.R
import com.killua.ideenplattform.data.models.Idea
import com.killua.ideenplattform.data.models.User
import com.killua.ideenplattform.databinding.ItemIdeaBinding

class IdeaViewHolder(
    val binding: ItemIdeaBinding,
    private val ideaOnClick: IdeaOnClick,
    private val currentUser: User
) :
    RecyclerView.ViewHolder(binding.root) {
    private val context = binding.root.context
    private val popup = reactionPopup(context) {
        reactions {
            resId { R.drawable.ic_angry_emoji }
            resId { R.drawable.ic_embarrassed }
            resId { R.drawable.ic_fun_giggles_emoji }
            resId { R.drawable.ic_chuckle_emoji }
            resId { R.drawable.ic_amour_emoji }

        }
        popupGravity = PopupGravity.DEFAULT
        popupMargin = context.resources.getDimensionPixelSize(R.dimen._35sdp)
        val cornerSizeInDp = 35
        popupCornerRadius =
            TypedValue.applyDimension(
                COMPLEX_UNIT_DIP,
                cornerSizeInDp.toFloat(),
                context.resources.displayMetrics
            ).toInt()
        popupColor = Color.WHITE

        reactionTexts = R.array.rating_data
        popupCornerRadius = 40
        popupColor = Color.LTGRAY
        popupAlpha = 255
        reactionSize = context.resources.getDimensionPixelSize(R.dimen._35sdp)
        horizontalMargin = context.resources.getDimensionPixelSize(R.dimen._4sdp)
        verticalMargin = horizontalMargin / 2
    }


    fun bind(item: Idea) {

        val didILiked = item.ratings.firstOrNull { it.user == currentUser }
        val currentRate = didILiked?.rating ?: -2
        updateLikeImage(currentRate)
        binding.tvComment.setOnClickListener { ideaOnClick.clicked(item.id) }
        binding.tvTitle.setOnClickListener { ideaOnClick.clicked(item.id) }
        binding.tvDescription.setOnClickListener { ideaOnClick.clicked(item.id) }
        binding.postImage.setOnClickListener { ideaOnClick.clicked(item.id) }

        popup.reactionSelectedListener = object : ReactionSelectedListener {
            override fun invoke(position: Int): Boolean = true.also {
                if (position == 0)
                    likeButtonSetUp(item, 1)
                else likeButtonSetUp(item, position + 1)
                Log.e("emoji", "${position + 1}")
            }
        }
        setupButtons(item)
        with(binding) {


            released.visibility = when {
                item.released -> View.GONE
                currentUser.isManager -> View.VISIBLE
                else -> View.GONE
            }
            data = item
            executePendingBindings()
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setupButtons(item: Idea) {
        with(binding) {
            rate.setOnTouchListener { v, event ->
                // Resolve reactions selection
                popup.onTouch(v, event)
            }
            imageProfile.setOnClickListener {
                if (item.author != currentUser) ideaOnClick.dialogProfileShow(item.author)
            }
            tvFullName.setOnClickListener {
                if (item.author != currentUser) ideaOnClick.dialogProfileShow(item.author)
            }

            tvRateDescription.text =
                root.context.resources.getQuantityString(
                    R.plurals.rated_count,
                    item.ratings.count(),
                    item.ratings.count()
                )
            tvComment.text =
                root.context.resources.getQuantityString(
                    R.plurals.comments_cont,
                    item.comments.size,
                    item.comments.size
                )
            tvRateDescription.setOnClickListener {
                ideaOnClick.dialogRatingShow(item.ratings)
            }
        }
    }

    private fun likeButtonSetUp(idea: Idea, emojiTag: Int = -1) {
        val didILiked = idea.ratings.firstOrNull { it.user == currentUser }
        val currentRate = didILiked?.rating ?: -2
        when {
            emojiTag < 0 -> {
                binding.rate.setImageDrawable(
                    AppCompatResources.getDrawable(
                        context,
                        R.drawable.ic_like
                    )
                )
                ideaOnClick.emojiOnClickInsert(emojiTag, idea.id, this)
                updateLikeImage(emojiTag)

            }
            emojiTag == currentRate -> {
                ideaOnClick.emojiOnClickRemove(idea.id, this)
                updateLikeImage(-2)

            }
            emojiTag > 0 -> {
                ideaOnClick.emojiOnClickInsert(emojiTag, idea.id, this)
                updateLikeImage(emojiTag)

            }
            else -> return
        }


    }

    private fun updateLikeImage(rate: Int) {
        binding.rate.setImageDrawable(
            when (rate) {
                1 -> AppCompatResources.getDrawable(context, R.drawable.ic_angry_emoji)
                2 -> AppCompatResources.getDrawable(context, R.drawable.ic_embarrassed)
                3 -> AppCompatResources.getDrawable(context, R.drawable.ic_fun_giggles_emoji)
                4 -> AppCompatResources.getDrawable(context, R.drawable.ic_chuckle_emoji)
                5 -> AppCompatResources.getDrawable(context, R.drawable.ic_amour_emoji)
                else -> AppCompatResources.getDrawable(context, R.drawable.ic_like)
            }
        )
    }
}
