package com.killua.ideenplattform.ideamain.home

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.killua.ideenplattform.MainActivity
import com.killua.ideenplattform.R
import com.killua.ideenplattform.databinding.FragmentHomeBinding
import kotlinx.coroutines.flow.collect
import org.koin.androidx.viewmodel.ext.android.viewModel
import android.view.inputmethod.EditorInfo
import android.widget.SearchView
import androidx.fragment.app.Fragment
import com.killua.ideenplattform.ideamain.uiutils.*


class HomeFragment : Fragment() {
    private val viewModel by viewModel<HomeViewModel>()
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        binding.viewModel = viewModel
        binding.addIdea = HomeAction.AddIdea
        binding.refreshLayout.setOnRefreshListener {
            viewModel.setIntent(HomeAction.SetupFragment(false))
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        viewModel.setIntent(HomeAction.SetupFragment(false))
        with(viewModel) {
            lifecycleScope.launchWhenStarted {

                getViewEffects.collect {
                    homeEffect(it)
                }
            }
            lifecycleScope.launchWhenStarted {
                getStateUi.collect { state ->
                    setupStateUi(state)
                }
            }
            lifecycleScope.launchWhenCreated {
                getStateUiDb.collect {
                    binding.dataBinding = it
                    binding.executePendingBindings()
                }
            }
        }
    }

    private fun setupStateUi(state: HomeStateUi) {
        binding.refreshLayout.isRefreshing = state.isLoadingProgressBar
        (activity as MainActivity?)?.binding?.offLine?.visibility =
            if (state.internetConnectionError) View.VISIBLE else View.GONE
    }

    private fun homeEffect(it: HomeViewEffect) {
        when (it) {
            is HomeViewEffect.NavigateTo -> findNavController().safeNavigate(it.action)
            is HomeViewEffect.MakeToast -> showToast(it.message)
            HomeViewEffect.SmoothScroll -> {
                binding.rvIdeas.smoothScrollToPosition(0)
            }
            is HomeViewEffect.ShowUserDialog -> showUserProfileDialog(it.user)
            is HomeViewEffect.ShowRatingDialog -> showRatingDialog(it.ratings)
        }
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.main_menu, menu)

        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onPrepareOptionsMenu(menu: Menu) {
        super.onPrepareOptionsMenu(menu)

        val searchItem = menu.findItem(R.id.app_bar_search)
        val searchView: SearchView = searchItem.actionView as SearchView
        searchView.imeOptions = EditorInfo.IME_ACTION_DONE
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                viewModel.setIntent(HomeAction.SearchFilter(query))
                return false
            }
            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


}