package com.killua.ideenplattform.ideamain.home

import android.content.Context
import com.killua.ideenplattform.R
import com.killua.ideenplattform.data.models.Category
import com.killua.ideenplattform.data.models.Idea
import com.killua.ideenplattform.data.models.IdeaRating
import com.killua.ideenplattform.data.models.User
import com.killua.ideenplattform.data.repository.MainRepository
import com.killua.ideenplattform.data.requests.PostRating
import com.killua.ideenplattform.di.MyApplication
import com.killua.ideenplattform.ideamain.editprofile.EditProfileViewEffect
import com.killua.ideenplattform.ideamain.home.adapters.*
import com.killua.ideenplattform.ideamain.toprank.TopRankFragmentDirections
import com.killua.ideenplattform.ideamain.uiutils.BaseViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.runBlocking
import org.threeten.bp.LocalDateTime
import org.threeten.bp.ZoneOffset
import org.threeten.bp.format.DateTimeFormatter
import java.lang.ref.WeakReference
import java.util.*

class HomeViewModel(
    private val mainRepository: MainRepository,
) :
    BaseViewModel<HomeStateUiDb, HomeViewEffect, HomeStateUi
            >(HomeStateUi(), HomeStateUiDb()), IdeaOnClick, CategoryOnClick, SortItemOnClick {
    val context: Context by lazy {
        val ref = WeakReference(MyApplication.instance.applicationContext)
        ref.get()!!
    }
    private var isTopRanked = false

    init {
        var currentUser: User
        if (hasInternetConnection())
            launchCoroutine {
                mainRepository.getMe().collect { repoResult ->
                    repoResult.data?.let {
                        currentUser = it
                        val ideasAdapter = IdeasAdapter(this@HomeViewModel, currentUser)
                        val categoriesAdapter = CategoryAdapter(this@HomeViewModel)
                        val sortItemAdapter = SortItemAdapter(this@HomeViewModel)
                        reducerDB {
                            copy(
                                ideasAdapter = ideasAdapter,
                                categoriesAdapter = categoriesAdapter,
                                sortTypeAdapter = sortItemAdapter
                            )
                        }
                    }
                }
            } else reducer { copy(internetConnectionError = true) }

    }

    fun setIntent(action: HomeAction) {

        when (action) {
            is HomeAction.SetupFragment -> setupMain(action.isTopRank)


            is HomeAction.DetailsIdea -> setupDetailsIdeaAction(action)


            HomeAction.AddIdea -> if (!isTopRanked) postEffect(
                HomeViewEffect.NavigateTo(
                    HomeFragmentDirections.homeToAdd()
                )
            )

            is HomeAction.LoadIdeasWithId ->
                if (hasInternetConnection()) launchCoroutine {
                    if (context.getString(R.string.all_ideas) == action.ideaId)
                        setIdeasListAdapter(isTopRank = false)
                    else getWithId(action.ideaId)
                }
                else reducer { copy(internetConnectionError = true) }

            is HomeAction.RateIdea -> setupInsertRate(action)
            is HomeAction.RemoveIdeaRate -> setupRemoveRate(action)
            is HomeAction.SearchFilter -> searchForIdea(action.query)
        }
    }

    private fun searchForIdea(query: String?) {
        if(query.isNullOrEmpty())return
        if (!hasInternetConnection())return
            launchCoroutine {
                mainRepository.searchIdeal(query).collect{
                    if(it.data?.size!! >1)
                        reducerDB {
                            ideasAdapter?.submitList(lastUpdate(it.data.toMutableList()))
                            copy(ideasAdapter = ideasAdapter)
                        }
                    else postEffect(HomeViewEffect.MakeToast(R.string.nothing_found))
                }
            }
    }

    private fun setupMain(action: Boolean) {
        if (hasInternetConnection()) launchCoroutine {
            reducer { (copy(isLoadingProgressBar = true)) }
            setIdeasListAdapter(action)
            isTopRanked = action
            setCategoryListAdapter()
            getSortTypeArray()
            reducer { copy(isLoadingProgressBar = false) }
        }
        else reducer { copy(internetConnectionError = true) }

    }

    private fun setupDetailsIdeaAction(action: HomeAction.DetailsIdea) {

        if (isTopRanked)
            postEffect(
                HomeViewEffect.NavigateTo(
                    TopRankFragmentDirections.topToDetails(action.ideaId)
                )
            )
        else
            postEffect(
                HomeViewEffect.NavigateTo(
                    HomeFragmentDirections.homeToDetail(action.ideaId)
                )
            )
    }

    private fun setupRemoveRate(action: HomeAction.RemoveIdeaRate) {
        if (hasInternetConnection()) launchCoroutine {
            mainRepository.deleteRating(action.id).collect { repoResultRate ->
                if (repoResultRate.data == true) {
                    setIdeasListAdapter(isTopRanked)
                } else
                    postEffect(HomeViewEffect.MakeToast(R.string.error))
            }
        } else reducer { copy(internetConnectionError = true) }
    }


    private fun setupInsertRate(action: HomeAction.RateIdea) {
        if (hasInternetConnection()) launchCoroutine {
            mainRepository.postRating(action.id, PostRating(action.i))
                .collect { repoResultRate ->
                    if (repoResultRate.data == true) {
                        setIdeasListAdapter(isTopRanked)

                    } else
                        postEffect(HomeViewEffect.MakeToast(R.string.error))
                }
        } else reducer { copy(internetConnectionError = true) }

    }

    private suspend fun setIdeasListAdapter(
        isTopRank: Boolean,
    ) {
        mainRepository.getAllIdeas().collect { repoResult ->
            repoResult.data?.let { ideas ->
                var ideasList = createDate(ideas.toMutableList())
                if (isTopRank) ideasList = mostRated(ideas.toMutableList())
                reducerDB {
                    runBlocking(Dispatchers.Main) {
                        ideasAdapter!!.submitList(ideasList)
                        postEffect(HomeViewEffect.SmoothScroll)
                    }
                    copy(ideasAdapter = ideasAdapter)
                }
            }
        }
    }

    private suspend fun setCategoryListAdapter() {
        mainRepository.getAllCategories().collect { repoResultResult ->
            repoResultResult.data?.let { categoriesList ->
                reducerDB {
                    runBlocking(Dispatchers.Main) {
                        val allIdeasString = context.getString(R.string.all_ideas)
                        categoriesList.add(
                            0, Category(
                                allIdeasString,
                                allIdeasString,
                                allIdeasString
                            )
                        )
                        categoriesAdapter!!.submitList(categoriesList)
                    }
                    copy(categoriesAdapter = categoriesAdapter)
                }
            }
        }
    }

    private fun getSortTypeArray() {
        reducerDB {
            runBlocking(Dispatchers.Main) {
                val mySortTyp = context.resources.getStringArray(R.array.sort_type).toList()
                if (sortTypeAdapter != null) sortTypeAdapter!!.submitList(mySortTyp)
            }
            copy(sortTypeAdapter = sortTypeAdapter)
        }
    }

    private fun getWithId(id: String) {
        if (hasInternetConnection()) launchCoroutine {
            reducer { (copy(isLoadingProgressBar = true)) }
            mainRepository.getAllIdeas(id).collect { repoResult ->
                val ideaType = repoResult.data
                when {
                    ideaType.isNullOrEmpty() ->
                        postEffect(HomeViewEffect.MakeToast(message = R.string.error))
                    repoResult.isNetworkingData -> {
                        reducerDB {
                            runBlocking(Dispatchers.Main) {
                                val idListed =
                                    createDate(repoResult.data.toMutableList())?.filter { it.category.id == id }
                                ideasAdapter!!.submitList(idListed)
                                postEffect(HomeViewEffect.SmoothScroll)
                            }
                            copy(ideasAdapter = ideasAdapter)
                        }
                    }
                }
                reducer { copy(isLoadingProgressBar = false) }
            }
        }
        else reducer { copy(internetConnectionError = true) }

    }

    override fun clicked(ideaId: String) {
        setIntent(HomeAction.DetailsIdea(ideaId))
    }

    override fun emojiOnClickInsert(i: Int, id: String, viewHolder: IdeaViewHolder) {
        setIntent(HomeAction.RateIdea(i, id, viewHolder))

    }

    override fun emojiOnClickRemove(id: String, viewHolder: IdeaViewHolder) {
        setIntent(HomeAction.RemoveIdeaRate(id, viewHolder))
    }

    override fun dialogProfileShow(user: User) {
        postEffect(HomeViewEffect.ShowUserDialog(user))
    }

    override fun dialogRatingShow(ratings: List<IdeaRating>) {
        postEffect(HomeViewEffect.ShowRatingDialog(ratings))
    }

    override fun categoryClicked(ideaId: String) {
        setIntent(HomeAction.LoadIdeasWithId(ideaId))
    }


    private fun mostRated(list: MutableList<Idea>?) =
        list?.sortedByDescending { it.ratings.count() }


    private fun lessRated(list: MutableList<Idea>?) = list?.sortedBy { it.ratings.count() }


    private fun createDate(list: MutableList<Idea>?) = list?.sortedByDescending {
        val dateFormat =
            DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
        LocalDateTime.parse(it.created, dateFormat).toEpochSecond(ZoneOffset.UTC)
    }


    private fun lastUpdate(list: MutableList<Idea>?) = list?.sortedByDescending {
        val dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSZ")
        LocalDateTime.parse(it.lastUpdated, dateFormat).toEpochSecond(ZoneOffset.UTC)
    }


    private fun mostComments(list: MutableList<Idea>?) =
        list?.sortedByDescending { it.comments.count() }


    private fun lessComments(list: MutableList<Idea>?) = list?.sortedBy { it.comments.count() }

    private fun shuffle(list: MutableList<Idea>?) = list?.shuffled()


    override fun itemClicked(ideaId: String) {
        reducerDB {
            val adapter = ideasAdapter
            val newList = when (sortTypeAdapter?.currentList?.indexOf(ideaId)) {
                0 -> shuffle(adapter?.currentList)
                1 -> lessRated(adapter?.currentList)
                2 -> createDate(adapter?.currentList)
                3 -> lastUpdate(adapter?.currentList)
                4 -> mostComments(adapter?.currentList)
                5 -> lessComments(adapter?.currentList)
                else -> null
            }
            runBlocking(Dispatchers.Main) {
                ideasAdapter?.submitList(newList)
                postEffect(HomeViewEffect.SmoothScroll)
            }
            copy(ideasAdapter = ideasAdapter)
        }
    }
}


