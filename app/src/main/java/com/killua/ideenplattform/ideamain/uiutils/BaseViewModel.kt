package com.killua.ideenplattform.ideamain.uiutils

import android.content.Context
import android.net.ConnectivityManager
import android.net.ConnectivityManager.*
import android.net.NetworkCapabilities.*
import android.os.Build
import androidx.databinding.BaseObservable
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.killua.ideenplattform.di.MyApplication
import kotlinx.coroutines.Job
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.newSingleThreadContext

/***
 *

 * @constructor()
 * @param StateUiDataBinding : stateDataBinding
 * @param ViewEffect  :  HomeViewEffect
 * @param StateUi  : error handler usw
 * )
 *
 */
open class BaseViewModel<StateUiDataBinding, ViewEffect, StateUi>(instanceOfState: StateUi, instanceOfStateDataBinding: StateUiDataBinding) :
    ViewModel() {
    private val workedThread = newSingleThreadContext("joshy")
    private val _stateUiDb = MutableStateFlow(instanceOfStateDataBinding)
    private val _viewEffect = Channel<ViewEffect>(Channel.BUFFERED)
    private val _stateUi = MutableStateFlow(instanceOfState)

    val getStateUiDb: StateFlow<StateUiDataBinding> = _stateUiDb
    val getViewEffects: Flow<ViewEffect> = _viewEffect.receiveAsFlow()
    val getStateUi: StateFlow<StateUi> = _stateUi

    internal fun reducerDB(senders: StateUiDataBinding.() -> StateUiDataBinding) {
        viewModelScope.launch(newSingleThreadContext("zoldyck")) {
            _stateUiDb.value = _stateUiDb.value.senders()
        }
    }

    protected fun postEffect(effect: ViewEffect) = viewModelScope.launch(newSingleThreadContext("killua")) {
        _viewEffect.send(effect)
    }

    protected fun reducer(senders: StateUi.() -> StateUi) {
        viewModelScope.launch(newSingleThreadContext("hisoka")) {
            _stateUi.value = _stateUi.value.senders()
        }
    }

    protected fun launchCoroutine(block: suspend BaseViewModel<StateUiDataBinding, ViewEffect, StateUi>.() -> Unit): Job {
        return viewModelScope.launch(workedThread) {
            block()
        }
    }

    protected fun hasInternetConnection(): Boolean {
        val connectivityManager = MyApplication.instance.applicationContext.getSystemService(
            Context.CONNECTIVITY_SERVICE
        ) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val activeNetwork = connectivityManager.activeNetwork ?: return false
            val capabilities =
                connectivityManager.getNetworkCapabilities(activeNetwork) ?: return false
            return when {
                capabilities.hasTransport(TRANSPORT_WIFI) -> true
                capabilities.hasTransport(TRANSPORT_CELLULAR) -> true
                capabilities.hasTransport(TRANSPORT_ETHERNET) -> true
                else -> false
            }
        } else {
            connectivityManager.activeNetworkInfo?.run {
                return when (type) {
                    TYPE_WIFI -> true
                    TYPE_MOBILE -> true
                    TYPE_ETHERNET -> true
                    else -> false
                }
            }
        }
        return false
    }
}
