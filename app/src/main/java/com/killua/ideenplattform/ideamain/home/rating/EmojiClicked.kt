package com.killua.ideenplattform.ideamain.home.rating

import com.killua.ideenplattform.data.models.User

interface EmojiClicked {
    fun userImageClicked(user: User)
}