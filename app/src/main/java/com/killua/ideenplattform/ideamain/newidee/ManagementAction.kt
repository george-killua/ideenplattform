package com.killua.ideenplattform.ideamain.newidee

sealed class ManagementAction {
    data class SetupMe(val ideaId: String? = null) : ManagementAction()
   object SaveChanges : ManagementAction()
    object UploadImage:ManagementAction()
    object TextChanged  : ManagementAction()
}