package com.killua.ideenplattform.authorization.login

sealed class LoginAction {
    object LoginClicked : LoginAction()
    object RegisterClicked : LoginAction()
    object InputHasChanged : LoginAction()
}