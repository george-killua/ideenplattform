package com.killua.ideenplattform.authorization.login

import android.annotation.SuppressLint
import android.util.Log.e
import android.util.Patterns
import com.killua.ideenplattform.R
import com.killua.ideenplattform.data.repository.MainRepository
import com.killua.ideenplattform.ideamain.uiutils.BaseViewModel
import kotlinx.coroutines.flow.collect

class LoginViewModel(private val loginRepository: MainRepository) :
    BaseViewModel<LoginStateUiDb, LoginViewEffect, LoginStateUi>(
        LoginStateUi(),
        LoginStateUiDb()
    ) {
    init {
//        try to make sign in first time
        if (hasInternetConnection())
            launchCoroutine {
                reducer { copy(logoVisible = true) }
                loginRepository.getMe().collect {
                    if (it.data != null)
                        postEffect(LoginViewEffect.NavigateToIdeaActivity)
                    else
                        reducer { copy(logoVisible = false) }
                }
            }
        else
            reducer { copy(networkError = true) }
    }

    @SuppressLint("ResourceType")
    fun setIntent(action: LoginAction) {
        when (action) {
            is LoginAction.InputHasChanged -> loginDataChanged()
            LoginAction.LoginClicked -> login()
            LoginAction.RegisterClicked -> postEffect(LoginViewEffect.NavigateTo(R.id.login_to_register))
        }
    }

    private fun login() {
        reducer {
            copy(loadingProgressBar = true)
        }
        reducerDB {
            val temp = this
            if (hasInternetConnection())
                launchCoroutine {
                    loginRepository.login(temp.email.trim(), temp.password.trim())
                        .collect { repoResult ->
                            if (repoResult.data == true)
                                postEffect(LoginViewEffect.NavigateToIdeaActivity)
                            else {
                                postEffect(LoginViewEffect.MakeToast(R.string.login_failed))

                            }
                            reducer {
                                copy(loadingProgressBar = false, isDataValid = false)
                            }
                        }
                }
            else
                reducer { copy(loadingProgressBar = false, networkError = true) }

            copy()
        }
    }


    private fun loginDataChanged() {
        reducerDB {
            if (!isEmailValid(email)) {
                reducer { copy(emailError = R.string.email_error) }
            } else
                //i lt this one activate sign in buttons without check password regex
                if (!isPasswordValid(password)) {
                    reducer { copy(passwordError = R.string.password_error) }
                } else
                    reducer { LoginStateUi(isDataValid = true) }
            copy()
        }
    }

    private fun isEmailValid(email: String): Boolean {
        return email.isNotBlank() && Patterns.EMAIL_ADDRESS.matcher(email).matches()

    }

    private fun isPasswordValid(password: String): Boolean {
        return password.length > 5
    }


}