package com.killua.ideenplattform.authorization.register

import android.util.Patterns
import com.killua.ideenplattform.R
import com.killua.ideenplattform.authorization.login.LoginViewEffect
import com.killua.ideenplattform.data.repository.MainRepository
import com.killua.ideenplattform.data.requests.UserCreateReq
import com.killua.ideenplattform.di.MyApplication
import com.killua.ideenplattform.ideamain.uiutils.BaseViewModel
import kotlinx.coroutines.flow.collect

class RegisterViewModel(private val repo: MainRepository) :
    BaseViewModel<RegisterStateUiDb, RegisterViewEffect, RegisterStateUi>(
        RegisterStateUi(),
        RegisterStateUiDb()
    ) {
    fun setIntent(action: RegisterAction) {
        when (action) {
            is RegisterAction.InputHasChanged -> registerDataChanged()
            RegisterAction.RegisterClicked -> registerAccount()
        }
    }

    private fun registerAccount() {
        reducer { copy(loadingProgressBar = true) }
        reducerDB {
            if (hasInternetConnection())
                launchCoroutine {
                    repo.createUser(
                        UserCreateReq(
                            this@reducerDB.email.trim(),
                            this@reducerDB.firstName.trim(),
                            this@reducerDB.lastName.trim(),
                            this@reducerDB.password.trim()
                        )
                    ).collect { repoResult ->
                        if (repoResult.networkErrorMessage == "409 Conflict")
                            reducer {
                                copy(
                                    emailError = R.string.email_already_registered,
                                    isDataValid = false,
                                    loadingProgressBar = false
                                )
                            }
                        if (repoResult.data == true) {
                            postEffect(RegisterViewEffect.NavigateToLogin).also {
                                postEffect(RegisterViewEffect.MakeToast(R.string.created))
                            }
                        }
                    }
                }
            else
                reducer { copy(loadingProgressBar = false, networkError = true) }
            copy()
        }

    }

    private fun registerDataChanged() {
        reducerDB {
            if (!isNameValid(firstName)) {
                reducer {
                    RegisterStateUi(firstNameError = R.string.firstname_error)
                }
            } else
                if (!isNameValid(lastName.trim())) {
                    reducer {
                        RegisterStateUi(lastNameError = R.string.lastname_error)
                    }
                } else

                    if (!isEmailValid(email.trim())) {
                        reducer {
                            RegisterStateUi(emailError = R.string.email_error)
                        }
                    } else
                        if (!isPasswordValid(password.trim())) {
                            reducer {
                                RegisterStateUi(passwordError = R.string.password_error)
                            }
                        } else if (!isPasswordConfirmValid(password.trim(), passwordConfirm.trim())) {
                            reducer {
                                RegisterStateUi(passwordConfirmError = R.string.password_confirm_error)
                            }
                        } else
                            reducer {
                                RegisterStateUi(isDataValid = true)
                            }
            copy()
        }
    }

    private fun isNameValid(name: String): Boolean {
        return name.length > 2 && name.trim().matches(regex = Regex("[a-zA-zäöüÄÖÜß]+([ '-][a-zA-ZäöüÄÖÜß]+)*"))

    }

    private fun isEmailValid(email: String): Boolean {
        return email.isNotBlank() && Patterns.EMAIL_ADDRESS.matcher(email.trim()).matches()

    }

    private fun isPasswordValid(password: String): Boolean {
        return password.trim().length > 5&& password.trim().matches(Regex("^(?=.*?[A-ZäöüÄÖÜß])(?=.*?[a-zäöüÄÖÜß])(?=.*?[0-9])(?=.*?[#?!@\$%^&*-]).{8,}\$"))
    }

    private fun isPasswordConfirmValid(password: String, passwordConfirm: String): Boolean {
        return password == passwordConfirm
    }
}