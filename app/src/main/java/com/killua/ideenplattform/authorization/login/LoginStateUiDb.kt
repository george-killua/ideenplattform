package com.killua.ideenplattform.authorization.login

data class LoginStateUiDb(
    var email: String = "",
    var password: String = "",
)