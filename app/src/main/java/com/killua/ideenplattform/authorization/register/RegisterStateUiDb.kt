package com.killua.ideenplattform.authorization.register

data class RegisterStateUiDb(
    var firstName: String = "",
    var lastName: String = "",
    var email: String = "",
    var password: String = "",
    var passwordConfirm: String = ""
)
