package com.killua.ideenplattform.authorization.login

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.killua.ideenplattform.MainActivity
import com.killua.ideenplattform.authorization.AuthenticationActivity
import com.killua.ideenplattform.databinding.FragmentLoginBinding
import com.killua.ideenplattform.ideamain.uiutils.showToast
import kotlinx.coroutines.flow.collect
import org.koin.androidx.viewmodel.ext.android.viewModel

class LoginFragment : Fragment() {
    private val loginViewModel: LoginViewModel by viewModel()
    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!
    private val obj = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        }

        override fun afterTextChanged(s: Editable?) {
            loginViewModel.setIntent(LoginAction.InputHasChanged)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        with(binding) {
            vmLogin = loginViewModel
            loginClicked = LoginAction.LoginClicked
            registerClicked = LoginAction.RegisterClicked
            etEmail.editText?.addTextChangedListener(obj)
            etPassword.editText?.addTextChangedListener(obj)
        }
        return binding.root

    }

    @SuppressLint("ResourceType")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        (requireActivity() as AuthenticationActivity).supportActionBar?.hide()

        lifecycleScope.launchWhenCreated {
            loginViewModel.getViewEffects.collect {
                effectHandler(it)
            }
        }
        lifecycleScope.launchWhenCreated {
            loginViewModel.getStateUiDb.collect {
                binding.stateUiDb = it
                binding.executePendingBindings()
            }
        }

        lifecycleScope.launchWhenCreated {
            loginViewModel.getStateUi.collect {
                stateUiHandler(it)
            }
        }
    }

    private fun stateUiHandler(it: LoginStateUi) {
        with(binding) {
            btnLogin.isEnabled = it.isDataValid
            binding.progressBar.visibility =
                if (it.loadingProgressBar) VISIBLE else View.GONE
            it.emailError.let { errorMessage ->
                if (errorMessage != null)
                    etEmail.error = getString(errorMessage)
                etEmail.isErrorEnabled = errorMessage != null
            }
            it.passwordError.let { errorMessage ->
                if (errorMessage != null)
                    etPassword.error = getString(errorMessage)
                etPassword.isErrorEnabled = errorMessage != null
            }
            logoStarter.visibility = if (it.logoVisible) VISIBLE else View.GONE
            (activity as AuthenticationActivity?)?.binding?.offLine?.visibility =
                if (it.networkError) VISIBLE else View.GONE
        }
    }

    @SuppressLint("ResourceType")
    private fun effectHandler(it: LoginViewEffect) {
        when (it) {
            is LoginViewEffect.MakeToast -> showToast(it.message)
            is LoginViewEffect.NavigateTo -> findNavController().navigate(it.action)
            LoginViewEffect.NavigateToIdeaActivity -> navigateToIdeaActivity()
        }
    }

    private fun navigateToIdeaActivity() = activity?.let { authActivity ->
        val navy = Intent(
            authActivity,
            MainActivity::class.java
        )
        authActivity.finishAndRemoveTask()
        startActivity(navy)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}