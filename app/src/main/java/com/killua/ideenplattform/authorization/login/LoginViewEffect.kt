package com.killua.ideenplattform.authorization.login

import androidx.annotation.NavigationRes
import androidx.annotation.StringRes
import com.killua.ideenplattform.R

sealed class LoginViewEffect {
    object NavigateToIdeaActivity : LoginViewEffect()
    data class MakeToast(@StringRes val message: Int = R.string.internet_error) : LoginViewEffect()
    data class NavigateTo(@NavigationRes val action: Int) : LoginViewEffect()
}