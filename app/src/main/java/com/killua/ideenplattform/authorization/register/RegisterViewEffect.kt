package com.killua.ideenplattform.authorization.register

import androidx.annotation.StringRes
import com.killua.ideenplattform.R

sealed class RegisterViewEffect {
    data class MakeToast(@StringRes val message: Int = R.string.internet_error) :
        RegisterViewEffect()

    object NavigateToLogin : RegisterViewEffect()
}