package com.killua.ideenplattform.authorization.login

data class LoginStateUi(
    val emailError: Int? = null,
    val passwordError: Int? = null,
    val isDataValid: Boolean = false,
    val loadingProgressBar: Boolean = false,
    val logoVisible: Boolean = false,
    val networkError: Boolean = false
)
