package com.killua.ideenplattform.authorization.register

data class RegisterStateUi(
    val firstNameError: Int? = null,
    val lastNameError: Int? = null,
    val emailError: Int? = null,
    val passwordError: Int? = null,
    val passwordConfirmError: Int? = null,
    val isDataValid: Boolean = false,
    val loadingProgressBar: Boolean = false,
    val networkError: Boolean = false
)