package com.killua.ideenplattform.authorization.register

sealed class RegisterAction {
    object RegisterClicked : RegisterAction()
    object InputHasChanged : RegisterAction()
}