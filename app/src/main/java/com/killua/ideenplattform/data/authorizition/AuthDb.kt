package com.killua.ideenplattform.data.authorizition

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [TokenUser::class], version = 1, exportSchema = false)
abstract class AuthDb : RoomDatabase() {
    abstract fun tokenUserDao(): TokenUserDao
}