package com.killua.ideenplattform.data.requests

class CreateIdeeReq(
    val title: String,
    val categoryId: String,
    val description: String
)