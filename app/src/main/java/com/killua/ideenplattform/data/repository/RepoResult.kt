package com.killua.ideenplattform.data.repository

data class RepoResult<out T>(
    val data: T? = null,
    val isNetworkingData: Boolean = false,
    val networkErrorMessage: String? = null,
    val NoInternetConnection: Boolean = false
)