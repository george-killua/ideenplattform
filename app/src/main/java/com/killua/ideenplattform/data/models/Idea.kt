package com.killua.ideenplattform.data.models

data class Idea(
    val id: String = "",
    val author: User,
    val title: String = "",
    val released: Boolean,
    val category: Category,
    val description: String = "",
    val created: String = "",
    val lastUpdated: String = "",
    val comments: List<IdeaComment> = listOf(),
    val imageUrl: String = "",
    val ratings: List<IdeaRating> = listOf()
) {
    val imageNull = imageUrl.isBlank()
}


