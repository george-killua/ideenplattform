package com.killua.ideenplattform.data.models

//    The rating information of the idea.
data class IdeaRating(val user: User, val rating: Int)