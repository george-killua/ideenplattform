package com.killua.ideenplattform.data.authorizition

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class TokenUser(@PrimaryKey(autoGenerate = false) val uid: Int = 1,@ColumnInfo(name = "auth") val credential: String)

