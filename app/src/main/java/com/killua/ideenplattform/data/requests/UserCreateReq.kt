package com.killua.ideenplattform.data.requests

class UserCreateReq(
    val email: String,
    val firstname: String,
    val lastname: String,
    val password: String,
)