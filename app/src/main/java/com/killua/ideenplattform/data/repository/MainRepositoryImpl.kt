package com.killua.ideenplattform.data.repository

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.killua.ideenplattform.data.authorizition.TokenUser
import com.killua.ideenplattform.data.authorizition.TokenUserDao
import com.killua.ideenplattform.data.models.Idea
import com.killua.ideenplattform.data.network.ApiServices
import com.killua.ideenplattform.data.repository.NetworkResult.ResponseHandler.safeApiCall
import com.killua.ideenplattform.data.requests.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.withContext
import okhttp3.Credentials
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File
import java.lang.reflect.Type

class MainRepositoryImpl(
    var api: ApiServices,
    val context: Context,
    private val tokenDao: TokenUserDao
) :
    MainRepository {

    override suspend fun login(email: String, password: String): Flow<RepoResult<Boolean>> {
        withContext(Dispatchers.IO) {
            tokenDao.insertToken(TokenUser(credential = Credentials.basic(email, password)))
        }

        return flow {

            getMe().collect {
                emit(
                    RepoResult(
                        it.data != null,
                        isNetworkingData = it.isNetworkingData,
                        it.networkErrorMessage
                    )
                )
            }
        }.flowOn(Dispatchers.IO)
    }


    override suspend fun getAllUsers() =
        flow {
            when (val res = safeApiCall(api.getAllUsers())) {

                is NetworkResult.Success ->
                    emit(RepoResult(res.data, true))

                is NetworkResult.Error ->
                    emit(RepoResult(null, false, res.message))
            }
        }.flowOn(Dispatchers.IO)

    override suspend fun createUser(userCreateReq: UserCreateReq) =
        flow {
            when (val res = safeApiCall(api.createUser(userCreateReq))) {

                is NetworkResult.Success ->
                    emit(
                        RepoResult(
                            data = true,
                            isNetworkingData = true,
                            networkErrorMessage = "User add Successfully"
                        )
                    )

                is NetworkResult.Error ->
                    emit(
                        RepoResult(
                            data = false,
                            isNetworkingData = false,
                            networkErrorMessage = res.message
                        )
                    )
            }
        }.flowOn(Dispatchers.IO)

    override suspend fun updateUser(userCreateReq: UserCreateReq) =
        flow {
            when (val res = safeApiCall(api.updateUser(userCreateReq))) {

                is NetworkResult.Success ->
                    emit(
                        RepoResult(
                            data = true,
                            isNetworkingData = true,
                            networkErrorMessage = "User updated Successfully"
                        )
                    )

                is NetworkResult.Error ->
                    emit(
                        RepoResult(
                            data = false,
                            isNetworkingData = false,
                            networkErrorMessage = res.message
                        )
                    )
            }
        }.flowOn(Dispatchers.IO)

    override suspend fun getMe() = flow {
        when (val res = safeApiCall(api.getMe())) {
            is NetworkResult.Success -> {
                emit(RepoResult(res.data, true))
            }
            is NetworkResult.Error ->
                emit(
                    RepoResult(
                        data = null,
                        isNetworkingData = false,
                        networkErrorMessage = res.message
                    )
                )

        }

    }.flowOn(Dispatchers.IO)

    override suspend fun getUserId(id: String) =
        flow {
            when (val res = safeApiCall(api.getUserId(id))) {

                is NetworkResult.Success ->
                    emit(RepoResult(data = res.data, isNetworkingData = true))
                is NetworkResult.Error ->
                    emit(
                        RepoResult(
                            data = null,
                            isNetworkingData = false,
                            networkErrorMessage = res.message
                        )
                    )
            }
        }.flowOn(Dispatchers.IO)

    override suspend fun uploadUserImage(file: File): Flow<RepoResult<Boolean>> {

        // image of idea request
        val requestImage: RequestBody = file.asRequestBody("image/jpg".toMediaTypeOrNull())

        val image: MultipartBody.Part =
            requestImage.let { MultipartBody.Part.createFormData("files[0]", file.name, it) }

        return flow {
            when (val res = safeApiCall(api.uploadUserImage(image))) {
                is NetworkResult.Success ->
                    emit(RepoResult(data = true, isNetworkingData = true))
                is NetworkResult.Error ->
                    emit(
                        RepoResult(
                            data = false,
                            isNetworkingData = false,
                            networkErrorMessage = res.message
                        )
                    )
            }
        }.flowOn(Dispatchers.IO)

    }

    override suspend fun deleteImageOfUser() = flow {
        when (val res = safeApiCall(api.deleteImageOfUser())) {
            is NetworkResult.Success ->
                emit(
                    RepoResult(
                        data = true,
                        isNetworkingData = true,
                        networkErrorMessage = "image deleted Successfully"
                    )
                )
            is NetworkResult.Error ->
                emit(
                    RepoResult(
                        data = false,
                        isNetworkingData = false,
                        networkErrorMessage = res.message
                    )
                )
        }
    }.flowOn(Dispatchers.IO)

    override suspend fun updateMangerStatus(
        userId: String,
        updateManagerStatus: UpdateManagerStatus,
    ) = flow {
        when (val res = safeApiCall(api.updateMangerStatus(userId, updateManagerStatus))) {
            is NetworkResult.Success ->
                emit(
                    RepoResult(
                        data = true,
                        isNetworkingData = true,
                        networkErrorMessage = "rating deleted Successfully"
                    )
                )
            is NetworkResult.Error ->
                emit(
                    RepoResult(
                        data = false,
                        isNetworkingData = false,
                        networkErrorMessage = res.message
                    )
                )
        }
    }.flowOn(Dispatchers.IO)

    override suspend fun getAllCategories() =
        flow {
            when (val res = safeApiCall(api.getCategory())) {
                is NetworkResult.Success ->
                    emit(RepoResult(data = res.data, isNetworkingData = true))
                is NetworkResult.Error ->
                    emit(
                        RepoResult(
                            data = null,
                            isNetworkingData = false,
                            networkErrorMessage = res.message
                        )
                    )
            }
        }.flowOn(Dispatchers.IO)

    override suspend fun getCategoryWithId(id: String) =
        flow {
            when (val res = safeApiCall(api.getCategoryWithId(id))) {
                is NetworkResult.Success ->
                    emit(
                        RepoResult(
                            data = res.data,
                            isNetworkingData = true,
                            networkErrorMessage = "rating deleted Successfully"
                        )
                    )
                is NetworkResult.Error ->
                    emit(
                        RepoResult(
                            data = null,
                            isNetworkingData = false,
                            networkErrorMessage = res.message
                        )
                    )
            }
        }.flowOn(Dispatchers.IO)

    override suspend fun createNewIdea(
        createIdeeReq: CreateIdeeReq,
        file: File?
    ): Flow<RepoResult<Idea?>> {
//body of idea request
        val type: Type = object : TypeToken<CreateIdeeReq>() {}.type
        val stringBody = Gson().toJson(createIdeeReq, type)
        val requestBody = stringBody.toRequestBody("application/json".toMediaTypeOrNull())
// image of idea request
        val requestImage: RequestBody? = file?.asRequestBody("image/jpg".toMediaTypeOrNull())
        val image: MultipartBody.Part? =
            requestImage?.let { MultipartBody.Part.createFormData("files[0]", file.name, it) }
        val apiRequest = if (image == null) api.createNewIdea(requestBody)
        else api.createNewIdea(requestBody, image)
        return flow {
            when (val res = safeApiCall(apiRequest)) {
                is NetworkResult.Success ->
                    emit(RepoResult(data = res.data, isNetworkingData = true))
                is NetworkResult.Error ->
                    emit(
                        RepoResult(
                            data = null,
                            isNetworkingData = false,
                            networkErrorMessage = res.message
                        )
                    )
            }
        }.flowOn(Dispatchers.IO)
    }

    override suspend fun getAllIdeas() = flow {
        when (val res = safeApiCall(api.getAllIdeas())) {

            is NetworkResult.Success ->
                emit(RepoResult(res.data, true))
            is NetworkResult.Error ->
                emit(RepoResult(null, false, res.message))
        }
    }.flowOn(Dispatchers.IO)

    override suspend fun getAllIdeas(categoryId: String) = flow {
        when (val res = safeApiCall(api.getAllIdeas(categoryId))) {
            is NetworkResult.Success ->
                emit(RepoResult(res.data, true))
            is NetworkResult.Error ->
                emit(RepoResult(null, false, res.message))

        }
    }.flowOn(Dispatchers.IO)

    override suspend fun getIdeaWithId(ideaId: String) = flow {
        when (val res = safeApiCall(api.getIdeaWithId(ideaId))) {

            is NetworkResult.Success ->
                emit(RepoResult(res.data, true))
            is NetworkResult.Error ->
                emit(RepoResult(null, false, res.message))
        }
    }.flowOn(Dispatchers.IO)

    override suspend fun updateIdeaWithId(
        ideaId: String,
        createIdeeReq: CreateIdeeReq,
    ) = flow {
        when (val res = safeApiCall(api.updateIdeaWithId(ideaId, createIdeeReq))) {

            is NetworkResult.Success ->
                emit(RepoResult(data = true, isNetworkingData = true))
            is NetworkResult.Error ->
                emit(
                    RepoResult(
                        data = null,
                        isNetworkingData = false,
                        networkErrorMessage = res.message
                    )
                )

        }
    }.flowOn(Dispatchers.IO)

    override suspend fun deleteIdeaWithId(ideaId: String) = flow {
        when (val res = safeApiCall(api.deleteIdeaWithId(ideaId))) {

            is NetworkResult.Success ->
                emit(RepoResult(data = true, isNetworkingData = true))
            is NetworkResult.Error ->
                emit(
                    RepoResult(
                        data = null,
                        isNetworkingData = false,
                        networkErrorMessage = res.message
                    )
                )
        }
    }.flowOn(Dispatchers.IO)

    override suspend fun searchIdeal(searchText: String) = flow {
        when (val res = safeApiCall(api.searchIdeal(searchText))) {
            is NetworkResult.Success ->
                emit(RepoResult(res.data, true))
            is NetworkResult.Error ->
                emit(
                    RepoResult(
                        data = null,
                        isNetworkingData = false,
                        networkErrorMessage = res.message
                    )
                )
        }
    }.flowOn(Dispatchers.IO)

    override suspend fun releaseIdea(
        ideaId: String,
        releaseReq: IdeaReleaseReq,
    ) = flow {
        when (val res = safeApiCall(api.releaseIdea(ideaId, releaseReq))) {

            is NetworkResult.Success ->
                emit(RepoResult(data = true, isNetworkingData = true))
            is NetworkResult.Error ->
                emit(
                    RepoResult(
                        data = false,
                        isNetworkingData = false,
                        networkErrorMessage = res.message
                    )
                )
        }
    }.flowOn(Dispatchers.IO)


    override suspend fun createComment(
        ideaId: String,
        createCommentReq: CreateCommentReq,
    ) = flow {

        val res = safeApiCall(
            api.createComment(ideaId, createCommentReq)
        )

        when (res) {

            is NetworkResult.Success -> emit(RepoResult(data = true, isNetworkingData = true))

            is NetworkResult.Error -> emit(
                RepoResult(
                    data = false,
                    isNetworkingData = false,
                    networkErrorMessage = res.message
                )
            )
        }
    }.flowOn(Dispatchers.IO)

    override suspend fun getComments(ideaId: String) =
        flow {
            when (val res = safeApiCall(api.getComments(ideaId))) {

                is NetworkResult.Success -> emit(
                    RepoResult(
                        data = res.data,
                        isNetworkingData = true
                    )
                )
                is NetworkResult.Error -> emit(
                    RepoResult(
                        data = null,
                        isNetworkingData = false,
                        networkErrorMessage = res.message
                    )
                )
            }
        }.flowOn(Dispatchers.IO)

    override suspend fun deleteComments(
        ideaId: String,
        commentId: String,
    ) = flow {
        when (val res = safeApiCall(api.deleteComments(ideaId, commentId))) {

            is NetworkResult.Success -> emit(RepoResult(data = true, isNetworkingData = true))

            is NetworkResult.Error -> emit(
                RepoResult(
                    data = false,
                    isNetworkingData = false,
                    networkErrorMessage = res.message
                )
            )
        }
        getComments(ideaId)
    }.flowOn(Dispatchers.IO)

    override suspend fun postRating(
        ideaId: String,
        postRating: PostRating,
    ) = flow {
        when (val res = safeApiCall(api.postRating(ideaId, postRating))) {

            is NetworkResult.Success -> emit(RepoResult(data = true, isNetworkingData = true))

            is NetworkResult.Error -> emit(
                RepoResult(
                    data = false,
                    isNetworkingData = false,
                    networkErrorMessage = res.message
                )
            )
        }
        getAllIdeas()
    }.flowOn(Dispatchers.IO)

    override suspend fun deleteRating(ideaId: String) = flow {
        when (val res = safeApiCall(api.deleteRating(ideaId))) {

            is NetworkResult.Success -> emit(
                value = RepoResult(
                    data = true,
                    isNetworkingData = true
                )
            )

            is NetworkResult.Error -> emit(
                RepoResult(
                    data = false,
                    isNetworkingData = false,
                    networkErrorMessage = res.message
                )
            )
        }
        getAllIdeas()
    }.flowOn(Dispatchers.IO)

    override suspend fun uploadImageIdea(
        ideaId: String,
        file: File?
    ): Flow<RepoResult<Boolean>> {
        val requestImage: RequestBody? = file?.asRequestBody("image/jpg".toMediaTypeOrNull())
        val image: MultipartBody.Part? =
            requestImage?.let { MultipartBody.Part.createFormData("files[0]", file.name, it) }
        return flow {
            when (val res = safeApiCall(api.uploadImageIdea(ideaId, image))) {

                is NetworkResult.Success -> emit(
                    value = RepoResult(
                        data = true,
                        isNetworkingData = true
                    )
                )

                is NetworkResult.Error -> emit(
                    RepoResult(
                        data = false,
                        isNetworkingData = false,
                        networkErrorMessage = res.message
                    )
                )
            }
        }.flowOn(Dispatchers.IO)
    }

    override fun deleteUserFomCaching() {
        tokenDao.deleteToken()
    }
}