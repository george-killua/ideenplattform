package com.killua.ideenplattform.data.models

import okhttp3.OkHttpClient

data class HttpClientHelper(val okHttpClient: OkHttpClient)