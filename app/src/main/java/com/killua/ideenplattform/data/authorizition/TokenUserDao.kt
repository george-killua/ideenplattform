package com.killua.ideenplattform.data.authorizition

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.killua.ideenplattform.data.authorizition.TokenUser

@Dao
interface TokenUserDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertToken(tokenUser: TokenUser)

    @Query("SELECT * FROM tokenuser  LIMIT 1;")
    fun getToken(): TokenUser?

    @Query("delete from tokenuser")
    fun deleteToken()
}