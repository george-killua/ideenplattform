package com.killua.ideenplattform.data.models

import androidx.room.Entity
import androidx.room.PrimaryKey


data class Category(
    val id: String = "",
    val name_en: String = "",
    val name_de: String = ""
)