package com.killua.ideenplattform.data.repository

import com.killua.ideenplattform.data.models.Category
import com.killua.ideenplattform.data.models.Idea
import com.killua.ideenplattform.data.models.IdeaComment
import com.killua.ideenplattform.data.models.User
import com.killua.ideenplattform.data.requests.*
import kotlinx.coroutines.flow.Flow
import java.io.File

interface MainRepository {

    suspend fun login(email: String, password: String): Flow<RepoResult<Boolean>>
    suspend fun getAllUsers(): Flow<RepoResult<ArrayList<User>>>
    suspend fun createUser(userCreateReq: UserCreateReq): Flow<RepoResult<Boolean>>
    suspend fun updateUser(userCreateReq: UserCreateReq): Flow<RepoResult<Boolean>>
    suspend fun getMe(): Flow<RepoResult<User?>>
    suspend fun getUserId(id: String): Flow<RepoResult<User>>
    suspend fun uploadUserImage(file: File): Flow<RepoResult<Boolean>>
    suspend fun deleteImageOfUser(): Flow<RepoResult<Boolean>>
    suspend fun updateMangerStatus(
        userId: String,
        updateManagerStatus: UpdateManagerStatus,
    ): Flow<RepoResult<Boolean>>

    suspend fun getAllCategories(): Flow<RepoResult<ArrayList<Category>>>
    suspend fun getCategoryWithId(id: String): Flow<RepoResult<Category>>

    suspend fun createNewIdea(
        createIdeeReq: CreateIdeeReq,
        file: File?,
    ): Flow<RepoResult<Idea?>>

    suspend fun getAllIdeas(): Flow<RepoResult<List<Idea>>>
    suspend fun getAllIdeas(categoryId: String): Flow<RepoResult<List<Idea>>>
    suspend fun getIdeaWithId(ideaId: String): Flow<RepoResult<Idea>>
    suspend fun updateIdeaWithId(
        ideaId: String,
        createIdeeReq: CreateIdeeReq,
    ): Flow<RepoResult<Boolean>>

    suspend fun deleteIdeaWithId(ideaId: String): Flow<RepoResult<Boolean>>
    suspend fun searchIdeal(searchText: String): Flow<RepoResult<List<Idea>>>
    suspend fun releaseIdea(
        ideaId: String,
        releaseReq: IdeaReleaseReq,
    ): Flow<RepoResult<Boolean>>

    suspend fun createComment(
        ideaId: String,
        createCommentReq: CreateCommentReq,
    ): Flow<RepoResult<Boolean>>

    suspend fun getComments(ideaId: String): Flow<RepoResult<List<IdeaComment>>>
    suspend fun deleteComments(ideaId: String, commentId: String): Flow<RepoResult<Boolean>>
    suspend fun postRating(ideaId: String, postRating: PostRating): Flow<RepoResult<Boolean>>
    suspend fun deleteRating(ideaId: String): Flow<RepoResult<Boolean>>
    suspend fun uploadImageIdea(ideaId: String, file: File?): Flow<RepoResult<Boolean>>
    fun deleteUserFomCaching()
}