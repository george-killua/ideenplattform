package com.killua.ideenplattform.data.models

import com.google.gson.annotations.SerializedName

data class IdeaComment(
    val id: String,
    @SerializedName("author")
    val user: User,
    val message: String = "",
    val created: String
) {
    fun ownerComment(currentUser: User) = currentUser.email == user.email
}