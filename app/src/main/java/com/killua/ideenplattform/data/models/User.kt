package com.killua.ideenplattform.data.models

import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("id")
    val userId: String = "", val email: String = "",
    val firstname: String = "",
    val lastname: String = "",
    val profilePicture: String = "",
    val isManager: Boolean = false
) {
    val fullName: String
        get() = "$firstname $lastname"

}

