package com.killua.ideenplattform.data.repository

import retrofit2.Response

sealed class NetworkResult<T>(
    val data: T? = null,
    val message: String? = null,
) {
    class Success<T>(data: T?) : NetworkResult<T>(data)
    class Error<T>(message: String, data: T? = null) : NetworkResult<T>(data, message)

    object ResponseHandler {
        fun <T> safeApiCall(apiCall: Response<T>): NetworkResult<T> {
            try {
                if (apiCall.isSuccessful || apiCall.code() == 204 || apiCall.code() == 201)
                    return Success(apiCall.body())
                return error("${apiCall.code()} ${apiCall.message()}")
            } catch (e: Exception) {
                return error(e.message ?: e.toString())
            }
        }

        private fun <T> error(errorMessage: String): NetworkResult<T> =
            Error(errorMessage)


    }
}